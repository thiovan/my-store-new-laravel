<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryCost extends Model
{
    protected $table = 'delivery_cost';
    public $timestamps = false;
}
