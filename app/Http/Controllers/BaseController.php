<?php

namespace App\Http\Controllers;

use App\Stok;

use App\ErrorLog;
use App\Variable;
use Carbon\Carbon;
use App\TransaksiDetail;
use App\Http\Controllers\Controller;
use App\Produk;

class BaseController extends Controller
{
    public function getVarLogException()
    {
        $logException = Variable::where('param', 'app_log_exception')->first();
        if ($logException != null) {
            return $logException->value == 'true' ? true : false;
        }
        return false;
    }

    public function getVarMypayApiUrl()
    {
        $mypayApiUrl = Variable::where('param', 'app_mypay_api_url')->first();
        return $mypayApiUrl != null ? $mypayApiUrl->value : '';
    }

    public function getVarFirebaseNotification()
    {
        $firebaseNotification = Variable::where('param', 'app_firebase_notification')->first();
        if ($firebaseNotification != null) {
            return $firebaseNotification->value == 'true' ? true : false;
        }
        return false;
    }

    public function getVarFirebaseApiKey()
    {
        $firebaseApiKey = Variable::where('param', 'app_firebase_api_key')->first();
        return $firebaseApiKey != null ? $firebaseApiKey->value : '';
    }

    public function getVarExcludeProduct()
    {
        $excludeProduct = Variable::where('param', 'app_exclude_product')->first();
        return $excludeProduct != null ? explode(',', $excludeProduct->value) : 0;
    }

    public function getVarPrefixNotif()
    {
        $env = Variable::where('param', 'app_env')->first()->value;
        if ($env == 'production') {
            return [
                'user'      => 'mystore-user-',
                'courier'   => 'mystore-courier-',
                'merchant'  => 'mystore-merchant-'
            ];
        } else {
            return [
                'user'      => 'mystoredev-user-',
                'courier'   => 'mystoredev-courier-',
                'merchant'  => 'mystoredev-merchant-'
            ];
        }
    }

    public function getVarClientId()
    {
        $clientId = Variable::where('param', 'app_client_id')->first();
        return $clientId != null ? $clientId->value : 0;
    }

    public function getVarClientSecret()
    {
        $clientSecret = Variable::where('param', 'app_client_secret')->first();
        return $clientSecret != null ? $clientSecret->value : '';
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($message, $result)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data'    => $result,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $object = true, $code = 400)
    {
        if ($object) {
            $response = [
                'success' => false,
                'message' => $error,
                'data'    => (object) []
            ];
        } else {
            $response = [
                'success' => false,
                'message' => $error,
                'data'    => array()
            ];
        }

        return response()->json($response, $code);
    }

    public function sendException(\Exception $exception, $function, $file = 'MainController')
    {
        $message = $exception->getMessage() . ' at ' . $exception->getFile() . ':' . $exception->getLine() . "\r\n" . $exception->getTraceAsString();

        if ($this->getVarLogException()) {
            $errorLog = new ErrorLog;
            $errorLog->message = $message;
            $errorLog->file = $file;
            $errorLog->function = $function;
            $errorLog->datetime = Carbon::now();
            $errorLog->save();

            return response()->json((object) [], 422);
        }

        return response()->json([
            'file'      => $file,
            'function'  => $function,
            'message'   => $message
        ], 422);
    }

    public function productStock($id_product)
    {
        if (Produk::find($id_product) == null) {
            return 0;
        }
        $productStock = Stok::where('id_produk', $id_product)
            ->sum('jumlah_stok');
        $productSold = TransaksiDetail::where('id_produk', $id_product)
            ->sum('jumlah');
        return $productStock - $productSold;
    }
}
