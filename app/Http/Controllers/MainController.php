<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Image;
use File;
use QrCode;

use App\Http\Functions\SkuGenerator;
use App\Http\Functions\Validation;
use App\Http\Functions\TrxCode;
use App\Http\Functions\CurlRequest;
use App\Http\Functions\FirebaseNotification;

use App\Cart;
use App\Courier;
use App\Discount;
use App\Kategori;
use App\Merchant;
use App\Brand;
use App\Produk;
use App\ProdukDesription;
use App\ProdukImage;
use App\Stok;
use App\Transaksi;
use App\TransaksiDetail;
use App\User;
use App\Version;
use App\Variable;
use App\TextMessage;
use App\Banner;
use App\DeliveryCost;
use App\Review;
use App\Profile;

use App\Http\Resources\BrandResource;
use App\Http\Resources\CartResource;
use App\Http\Resources\KategoriResource;
use App\Http\Resources\MerchantResource;
use App\Http\Resources\ProdukResource;
use App\Http\Resources\TransaksiDetailResource;
use App\Http\Resources\TransaksiResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\CourierResource;
use App\Http\Resources\LoginMitraResource;
use App\Http\Resources\BannerResource;

class MainController extends BaseController
{
    private $exclude_product_id;
    private $prefix_notif_user;
    private $prefix_notif_courier;
    private $prefix_notif_merchant;

	public function __construct()
	{
        $this->exclude_product_id = $this->getVarExcludeProduct();
        $this->prefix_notif_user = $this->getVarPrefixNotif()['user'];
        $this->prefix_notif_courier = $this->getVarPrefixNotif()['courier'];
        $this->prefix_notif_merchant = $this->getVarPrefixNotif()['merchant'];
	}

	public function coba()
	{

		return FirebaseNotification::sendOrder('title', 'message', '228', $this->prefix_notif_user.'087700326227');
		return FirebaseNotification::sendPromotion('title', 'message', 'http://satsetbatbet.com/my-store-dev/banner=453135', $this->prefix_notif_user.'087700326227');
		return FirebaseNotification::send('title', 'inner title', 'Produk yang dibeli:<br>asdasdad<br>asdadasd', $this->prefix_notif_user.'087700326227');
	}

	//START PRODUCT SECTION
	public function productSearch($keyword)
	{
		try {
			if ($keyword == 'TEST123') {
				$resultProduks = Produk::where('id', '160')
				->get();
			} else {
				$resultProduks = Produk::where('nama_produk', 'like', '%' . $keyword . '%')
				->whereNotIn('id', ['160'])
				->orderBy('priority', 'desc')
				->get();
			}

			$resultMerchants = Merchant::where('nama_merchant', 'like', '%' . $keyword . '%')->get();

			$response = [
				'produk_result'		=> ProdukResource::collection($resultProduks),
				'merchant_result'	=> MerchantResource::collection($resultMerchants)
			];

			return $this->sendResponse('Search result retrieved successfully', $response);
		} catch (\Exception $e) {
			return $this->sendException($e, 'productSearch');
		}
	}

	public function productByCategory($id_kategori)
	{
		try {
			$Produks = Produk::where('id_kategori', $id_kategori)
			->whereNotIn('id', $this->exclude_product_id)
			->orderBy('priority', 'desc')
			->get();

			return $this->sendResponse('Product list by category retrieved successfully', ProdukResource::collection($Produks));
		} catch (\Exception $e) {
			return $this->sendException($e, 'productListByCategory');
		}
	}

	public function productByBrand($id_brand)
	{
		try {
			$Produks = Produk::where('id_parent', $id_brand)
			->whereNotIn('id', $this->exclude_product_id)
			->orderBy('priority', 'desc')
			->get();

			return $this->sendResponse('Product list by brand retrieved successfully', ProdukResource::collection($Produks));
		} catch (\Exception $e) {
			return $this->sendException($e, 'productListByCategory');
		}
	}

	public function productBySKU($sku)
	{
		try {
			$Produk = Produk::where('sku', $sku)->first();

			if ($Produk != null) {
				return $this->sendResponse('Product show by sku retrieved successfully', new ProdukResource($Produk));
			}else{
				return $this->sendError('Product not found');
			}
		} catch (\Exception $e) {
			return $this->sendException($e, 'productShowBySKU');
		}
	}

	public function productById($id_produk)
	{
		try {
			$Produk = Produk::find($id_produk);

			if ($Produk != null) {
				return $this->sendResponse('Product show by id retrieved successfully', new ProdukResource($Produk));
			}else{
				return $this->sendError('Product not found');
			}
		} catch (\Exception $e) {
			return $this->sendException($e, 'productShowById');
		}
	}

	public function productByMerchant($id_merchant)
	{
		try {
			$Produks = Produk::where('id_merchant', $id_merchant)->whereNotIn('id', $this->exclude_product_id)->orderBy('priority', 'desc')->get();

			return $this->sendResponse('Product list by merchant retrieved successfully', ProdukResource::collection($Produks));
		} catch (\Exception $e) {
			return $this->sendException($e, 'productListByMerchant');
		}
	}

	public function productUpdate(Request $request, $id_produk)
	{
		try {
			$Produk = Produk::find($id_produk);
			$Produk->nama_produk = $request->nama_produk;
			$Produk->harga = $request->harga;
			$Produk->id_kategori = $request->id_kategori;
			$Produk->id_parent = $request->id_parent;
			if (substr($Produk->sku, 0, 2) != 'TS') {
				$Produk->sku = $request->sku;
			}
			$Produk->save();

			return $this->sendResponse('Product updated successfully', new ProdukResource($Produk));
		} catch (\Exception $e) {
			return $this->sendException($e, 'productUpdate');
		}
	}

	public function productBestSales()
	{
		try {
			$BestSales = DB::table('produk')
			->select(DB::raw('produk.*, (SELECT SUM(jumlah) FROM transaksidetail WHERE id_produk = produk.id) as penjualan'))
			->whereNotIn('id', $this->exclude_product_id)
			->orderBy('penjualan', 'desc')
			->limit(5)
			->get();

			return $this->sendResponse('Best sales product retrieved successfully', ProdukResource::collection($BestSales));
		} catch (\Exception $e) {
			return $this->sendException($e, 'productBestSales');
		}
	}

	public function productDelete($id_produk)
	{
		try {
			$Produk = Produk::destroy($id_produk);

			return $this->sendResponse('Product deleted successfully', $Produk);
		} catch (\Exception $e) {
			return $this->sendException($e, 'productDelete');
		}
	}
	//END PRODUCT SECTION

	//START CART SECTION
	public function cartByUser(Request $request, $id_user)
	{
		try {
			$Carts = Cart::where('machine', $id_user)->get();

			return $this->sendResponse('Shopping cart retrieved successfully', CartResource::collection($Carts));
		} catch (\Exception $e) {
			return $this->sendException($e, 'cartList');
		}
	}

	public function cartById(Request $request, $id_cart)
	{
		try {
			$Carts = Cart::find(explode(',', $id_cart));

			return $this->sendResponse('Shopping cart retrieved successfully', CartResource::collection($Carts));
		} catch (\Exception $e) {
			return $this->sendException($e, 'cartList');
		}
	}

	public function cartStore(Request $request)
	{
		try {
			$CartTemp = Cart::where('machine', $request->id_user)->where('id_produk', $request->id_produk)->first();

			if ($CartTemp != null) {
				$Cart = Cart::find($CartTemp->id);
				$Cart->jumlah = $Cart->jumlah + $request->jumlah;
				$Cart->save();
			}else{
				$Cart = new Cart;
				$Cart->id_produk = $request->id_produk;
				$Cart->harga = $request->harga;
				$Cart->jumlah = $request->jumlah;
				$Cart->machine = $request->id_user;
				$Cart->save();
			}

			return $this->sendResponse('Shopping cart stored successfully', new CartResource($Cart));
		} catch (\Exception $e) {
			return $this->sendException($e, 'cartStore');
		}
	}

	public function cartUpdate(Request $request, $id_cart)
	{
		try {
			$Cart = Cart::find($id_cart);
			$Cart->jumlah = $request->jumlah;
			$Cart->save();

			return $this->sendResponse('Shopping cart updated successfully', new CartResource($Cart));
		} catch (\Exception $e) {
			return $this->sendException($e, 'cartUpdate');
		}
	}

	public function cartDelete($id_cart)
	{
		try {
			$Cart = Cart::destroy($id_cart);

			return $this->sendResponse('Shopping cart deleted successfully', $Cart);
		} catch (\Exception $e) {
			return $this->sendException($e, 'cartDelete');
		}
	}

	public function cartCount(Request $request, $id_user)
	{
		try {
			$cartCount = Cart::where('machine', $id_user)->count();

			return $this->sendResponse('Shopping cart count retrieved successfully', ['cart_count' => $cartCount]);
		} catch (\Exception $e) {
			return $this->sendException($e, 'cartCount');
		}
	}
	//END CART SECTION

	//START TRANSACTION SECTION
	public function transactionStore(Request $request)
	{
		try {
			//LOGGING
			$fp = fopen('transaction.log', 'a');
			fwrite($fp, PHP_EOL . PHP_EOL . Carbon::now() . PHP_EOL);
			fwrite($fp, 'REQUEST DATA' . PHP_EOL);
			fwrite($fp, 'Id cart: ' . implode("|",$request->id_cart) . PHP_EOL);
			fwrite($fp, 'Id user: ' . $request->id_user . PHP_EOL);
			fwrite($fp, 'Pin: ' . 'secret' . PHP_EOL);
			fwrite($fp, 'Address: ' . $request->address . PHP_EOL);
			fwrite($fp, 'Remark: ' . $request->remark . PHP_EOL);
			fwrite($fp, 'Lat: ' . $request->lat . PHP_EOL);
			fwrite($fp, 'Lon: ' . $request->lon . PHP_EOL);
			//LOGGING

			$tempCarts = Cart::leftJoin('produk', 'cart.id_produk', '=', 'produk.id')
			->leftJoin('merchant', 'produk.id_merchant', '=', 'merchant.id')
			->whereIn('cart.id', $request->id_cart)
			->get([
				'merchant.no_telp as no_telp',
				'produk.id_merchant as id_merchant',
				'cart.harga as harga',
				'cart.jumlah as jumlah',
				'cart.id_produk as id_produk'
			]);

			$tempUser = User::with('profile')->find($request->id_user);
			$tempMerchantPhone = [];
			$tempIdMerchant = [];
			$tempTotal = 0;
			foreach ($tempCarts as $Cart) {
				array_push($tempMerchantPhone, $Cart->no_telp);
				array_push($tempIdMerchant, $Cart->id_merchant);
				$tempTotal = $tempTotal + ($Cart->harga * $Cart->jumlah);

				//Start check stock
				$productStock = Stok::where('id_produk', $Cart->id_produk)->sum('jumlah_stok') - TransaksiDetail::where('id_produk', $Cart->id_produk)->sum('jumlah');
				$isReady = Produk::find($Cart->id_produk)->is_ready;
				if ($Cart->jumlah > $productStock && $isReady == 'y') {
					//LOGGING
					fwrite($fp, 'STOCK NOT ENOUGH' . PHP_EOL);
					fwrite($fp, 'Cart count: ' . $Cart->jumlah . PHP_EOL);
					fwrite($fp, 'Available stock: ' . $productStock . PHP_EOL);
					fclose($fp);
					//LOGGING
					return $this->sendError('Stok produk tidak cukup, mohon kurangi jumlah pembelian');
				}
				//End check stock
			}
			$tempIdMerchant = array_unique($tempIdMerchant);

			//LOGGING
			fwrite($fp, 'Id merchant: ' . implode("|",$tempIdMerchant) . PHP_EOL);
			fwrite($fp, 'User balance: ' . $tempUser->profile->balance . PHP_EOL);
			fwrite($fp, 'Total: ' . $tempTotal . PHP_EOL);
			//LOGGING

			//Start check if merchant buy own product
			if (in_array($tempUser->phone, $tempMerchantPhone)) {
				//LOGGING
				fwrite($fp, 'BUY OWN PRODUCT' . PHP_EOL);
				fwrite($fp, 'Buyer phone: ' . $tempUser->phone . PHP_EOL);
				fwrite($fp, 'Merchant phone: ' . implode("|",$tempMerchantPhone) . PHP_EOL);
				fclose($fp);
				//LOGGING
				return $this->sendError('Tidak diperbolehkan membeli produk anda sendiri');
			}
			//End check if merchant buy own product

			//Start check balance
			if ($tempUser->profile->balance < $tempTotal) {
				//LOGGING
				fwrite($fp, 'BALANCE NOT ENOUGH' . PHP_EOL);
				fwrite($fp, 'User balance: ' . $tempUser->profile->balance . PHP_EOL);
				fwrite($fp, 'Total: ' . $tempTotal . PHP_EOL);
				fclose($fp);
				//LOGGING
				return $this->sendError('Saldo anda tidak cukup');
			}
			//End check balance

			//Start check pin
			$url = $this->getVarMypayApiUrl() . 'auth/accounts/pin';
			$params = [
				'pin'	=> $request->pin
			];
			$headers = [
				'Accept: application/json',
				'X-Requested-With: XMLHttpRequest',
				'Authorization: ' . $request->header('Authorization'),
				'Content-Type: multipart/form-data'
			];

			$response = CurlRequest::postWithHeader($url, $params, $headers);
			if ($response != 200) {
				//LOGGING
				fwrite($fp, 'PIN INVALID' . PHP_EOL);
				fwrite($fp, json_encode($response) . PHP_EOL);
				fclose($fp);
				//LOGGING
				return $this->sendError('Pin yang anda masukkan salah');
			}
			//End check pin

			$savedTransaction = [];
			foreach ($tempIdMerchant as $idMerchant) {
				//LOGGING
				fwrite($fp, 'HANDLING TRANSACTION MERCHANT ' . $idMerchant . PHP_EOL);
				//LOGGING
				$Carts = Cart::join('produk', 'cart.id_produk', '=', 'produk.id')
				->whereIn('cart.id', $request->id_cart)
				->where('produk.id_merchant', $idMerchant)
				->get(['cart.id as id', 'cart.harga as harga', 'cart.jumlah as jumlah', 'cart.id_produk as id_produk', 'produk.nama_produk as nama']);

				$idCarts = [];
				$nominal = 0;
				foreach ($Carts as $Cart) {
					$nominal = $nominal + ($Cart->harga * $Cart->jumlah);
					array_push($idCarts, $Cart->id);
				}

				$DeliveryCost = DeliveryCost::where('id_merchant', $idMerchant)->first();

				$Transaksi = new Transaksi;
				$Transaksi->no_ref = TrxCode::generate($idMerchant);
				$Transaksi->nominal = $nominal;
				$Transaksi->waktu = Carbon::now();
				$Transaksi->id_text = '0';
				$Transaksi->id_courier = '0';
				$Transaksi->address = $request->address;
				if ($request->remark == null) {
					$Transaksi->remark = '-';
				}else{
					$Transaksi->remark = $request->remark;
				}
				if ($DeliveryCost == null) {
					$Transaksi->delivery_cost = 0;
				}else{
					$Transaksi->delivery_cost = $DeliveryCost->cost;
				}
				$Transaksi->status = '0';
				$Transaksi->lat = $request->lat;
				$Transaksi->lon = $request->lon;
				$Transaksi->machine = $request->id_user;

				$successIds = [];
				if ($Transaksi->save()) {
					//LOGGING
					fwrite($fp, 'Transaction #' . $Transaksi->id . ' Stored' . PHP_EOL);
					//LOGGING
					for ($i=0; $i < count($Carts); $i++) {
						$TransaksiDetail = new TransaksiDetail;
						$TransaksiDetail->id_transaksi = $Transaksi->id;
						$TransaksiDetail->id_produk = $Carts[$i]->id_produk;
						$TransaksiDetail->harga = $Carts[$i]->harga;
						$TransaksiDetail->jumlah = $Carts[$i]->jumlah;
						$TransaksiDetail->waktu = $Transaksi->waktu;
						$TransaksiDetail->machine = $request->id_user;
						$TransaksiDetail->mdr = Merchant::find($idMerchant)->mdr;


						if (!$TransaksiDetail->save()) {
							//Rollback saved data
							Transaksi::destroy($Transaksi->id);
							foreach ($successIds as $successId) {
								TransaksiDetail::destroy($successId);
							}

							//LOGGING
							fwrite($fp, 'FAILED SAVING TRANSACTION DETAIL' . PHP_EOL);
							fwrite($fp, 'id_transaksi: ' . $TransaksiDetail->id_transaksi . PHP_EOL);
							fwrite($fp, 'id_produk: ' . $TransaksiDetail->id_produk . PHP_EOL);
							fwrite($fp, 'harga: ' . $TransaksiDetail->harga . PHP_EOL);
							fwrite($fp, 'jumlah: ' . $TransaksiDetail->jumlah . PHP_EOL);
							fwrite($fp, 'waktu: ' . $TransaksiDetail->waktu . PHP_EOL);
							fwrite($fp, 'machine: ' . $TransaksiDetail->machine . PHP_EOL);
							fwrite($fp, 'mdr: ' . $TransaksiDetail->mdr . PHP_EOL);
							fclose($fp);
							//LOGGING
							return $this->sendError('Transaksi gagal, mohon coba beberapa saat lagi (code: 0x001)');
						}

						//LOGGING
						fwrite($fp, 'TransactionDetail #' . $TransaksiDetail->id . ' Stored' . PHP_EOL);
						//LOGGING
						array_push($successIds, $TransaksiDetail->id);
					}

				}else{
					//LOGGING
					fwrite($fp, 'TRANSACTION STORE FAILED' . PHP_EOL);
					fwrite($fp, 'no_ref: ' . $Transaksi->no_ref . PHP_EOL);
					fwrite($fp, 'nominal: ' . $Transaksi->nominal . PHP_EOL);
					fwrite($fp, 'waktu: ' . $Transaksi->waktu . PHP_EOL);
					fwrite($fp, 'id_text: ' . $Transaksi->id_text . PHP_EOL);
					fwrite($fp, 'id_courier: ' . $Transaksi->id_courier . PHP_EOL);
					fwrite($fp, 'address: ' . $Transaksi->address . PHP_EOL);
					fwrite($fp, 'remark: ' . $Transaksi->remark . PHP_EOL);
					fwrite($fp, 'delivery_cost: ' . $Transaksi->delivery_cost . PHP_EOL);
					fwrite($fp, 'status: ' . $Transaksi->status . PHP_EOL);
					fwrite($fp, 'lat: ' . $Transaksi->lat . PHP_EOL);
					fwrite($fp, 'lon: ' . $Transaksi->lon . PHP_EOL);
					fwrite($fp, 'machine: ' . $Transaksi->machine . PHP_EOL);
					fclose($fp);
					//LOGGING
					return $this->sendError('Transaksi gagal, mohon coba beberapa saat lagi (code: 0x002)');
				}

				//Start transfer to merchant
				$Merchant = Merchant::find($idMerchant);
				$url = $this->getVarMypayApiUrl() . 'payment/mystore';
				$params = [
					'merchant_number' 	=> $Merchant->no_telp,
					'amount'			=> (int)$nominal,
					'pin'				=> $request->pin,
					'mdr'				=> (int)$Merchant->mdr,
					'ongkir'			=> (int)$Transaksi->delivery_cost
				];
				$headers = [
					'Accept: application/json',
					'X-Requested-With: XMLHttpRequest',
					'Authorization: ' . $request->header('Authorization'),
					'Content-Type: multipart/form-data'
				];

				//LOGGING
				fwrite($fp, 'Amount: ' . json_encode($params) . PHP_EOL);
				//LOGGING

				$response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
				if ($response['status'] != 200) {
					//Rollback saved data
					Transaksi::destroy($Transaksi->id);
					foreach ($successIds as $successId) {
						TransaksiDetail::destroy($successId);
					}

					if (isset($response['response']->errors)) {
						//LOGGING
						fwrite($fp, 'ERROR RESPONSE FROM payment/mystore message: ' . json_encode($response) . PHP_EOL);
						fclose($fp);
						//LOGGING

						if (isset($response['response']->errors->message)) {
							return $this->sendError($response['response']->errors->message);
						} else if (isset($response['response']->errors->amount)) {
							return $this->sendError($response['response']->errors->amount);
						} else {
							return $this->sendError('Unhandled error');
						}

					}

					//LOGGING
					fwrite($fp, 'INVALID RESPONSE FROM payment/mystore code: ' . $response['status'] . PHP_EOL);
					fclose($fp);
					//LOGGING
					return $this->sendError('Gagal memotong saldo, mohon coba beberapa saat lagi');
				}
				$Transaksi = Transaksi::find($Transaksi->id);
				$Transaksi->no_ref = $response['response']->transaction_number;
				$Transaksi->save();
				//End transfer to merchant

				array_push($savedTransaction, $Transaksi->id);

				//Start send notification buyer
				$title = 'Status Transaksi #' . $Transaksi->no_ref;
				$message = 'Transaksi anda sedang diproses';
				$idTransaction = $Transaksi->id;
				$topics = $this->prefix_notif_user.$tempUser->phone;
				FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
				//End send notification buyer

				//Start send notification merchant
				$title = 'Order Masuk #' . $Transaksi->no_ref;
				$inner_title = 'Transaksi menunggu konfirmasi';
				$message = '';
				$topics = $this->prefix_notif_merchant.$Merchant->no_telp;
				FirebaseNotification::send($title, $inner_title, $message, $topics);
				//End send notification merchant

				Cart::destroy($idCarts);
			}

			//LOGGING
			fwrite($fp, 'TRANSACTION SUCCESS' . PHP_EOL);
			//LOGGING
			return $this->sendResponse('Transaction stored successfully', ['id_transaction' => $savedTransaction]);
		} catch (\Exception $e) {
			return $this->sendException($e, 'transactionStore');
		}
	}

	public function transactionById(Request $request, $id_transaction)
	{
		try {
			$Transaksi = Transaksi::find($id_transaction);

			return $this->sendResponse('Transaction history retrieved successfully', new TransaksiResource($Transaksi));
		} catch (\Exception $e) {
			return $this->sendException($e, 'transactionById');
		}
	}

	//Params: id_transaction, token, authpinsender
	public function transactionCancel(Request $request)
	{
		try {
			$Transaksi = Transaksi::find($request->id_transaction);
			$User = User::find($Transaksi->machine);

			if ($Transaksi->status != '0') {
				return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store');
			}

			if ($request->type == 'cancel') {
				$url = $this->getVarMypayApiUrl() . 'payment/mystore/cancel';
			} else if ($request->type == 'refund') {
				$url = $this->getVarMypayApiUrl() . 'payment/mystore/refund';
			}

			$params = [
				'transaction_number' => $Transaksi->no_ref,
			];
			$headers = [
				'Accept: application/json',
				'X-Requested-With: XMLHttpRequest',
				'Authorization: ' . $request->header('Authorization'),
				'Content-Type: multipart/form-data'
			];
			$response = CurlRequest::postMultipartWithHeader($url, $params, $headers);

			if ($response['status'] == 200) {
				$Transaksi->status = '-1';
				if ($Transaksi->save()) {
					if ($request->type == 'cancel') {

						$title = "Status Transaksi #" . $Transaksi->no_ref;
						$message = "Transaksi anda telah dibatalkan";
						$idTransaction = $Transaksi->id;
						$topics = $this->prefix_notif_user . $User->phone;
						FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
					} else if ($request->type == 'refund') {

						$title = "Status Transaksi #" . $Transaksi->no_ref;
						$message = "Transaksi anda dibatalkan oleh merchant";
						$idTransaction = $Transaksi->id;
						$topics = $this->prefix_notif_user . $User->phone;
						FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
					}

					return $this->sendResponse('Pesanan berhasil dibatalkan', '');
				} else {
					return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store');
				}
			} else {
				return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store');
			}

		} catch (\Exception $e) {
			return $this->sendException($e, 'cancelTransaction');
		}
	}

	public function history($id_user)
	{
		try {
			$Transaksis = Transaksi::where('machine', $id_user)
			->whereIn('status', ['-1', '3'])
			->orderBy('waktu', 'desc')
			->get();

			return $this->sendResponse('History retrieved successfully', TransaksiResource::collection($Transaksis));
		} catch (\Exception $e) {
			return $this->sendException($e, 'transactionItemsList');
		}
	}

	public function order($id_user)
	{
		try {
			$Transaksis = Transaksi::where('machine', $id_user)
			->whereIn('status', ['0', '1', '2'])
			->orderBy('waktu', 'desc')
			->get();

			return $this->sendResponse('Order retrieved successfully', TransaksiResource::collection($Transaksis));
		} catch (\Exception $e) {
			return $this->sendException($e, 'orderList');
		}
	}

	public function orderCount($id_user)
	{
		try {
			$orderCount = Transaksi::where('machine', $id_user)
			->whereIn('status', ['0', '1', '2'])
			->orderBy('waktu', 'desc')
			->count();

			return $this->sendResponse('Order retrieved successfully', ['order_count' => $orderCount]);
		} catch (\Exception $e) {
			return $this->sendException($e, 'orderCount');
		}
	}

	public function reviewStore(Request $request)
	{
		try {
			$Review 				= new Review;
			$Review->id_user 		= $request->id_user;
			$Review->id_transaksi 	= $request->id_transaction;
			$Review->rating 		= $request->rating;
			$Review->review 		= $request->review;

			if ($Review->save()) {
				return $this->sendResponse('Review stored successfully', $Review);
			}

			return $this->sendError('Review failed to store');
		} catch (\Exception $e) {
			return $this->sendException($e, 'reviewStore');
		}
	}
	//START TRANSACTION SECTION

	//START MITRA SECTION
	public function latestVersion()
	{
		try {
			$Version = Version::orderBy('id', 'desc')->first();

			return response()->json([
				'latestVersion' 	=> $Version->latest_version,
				'latestVersionCode' => $Version->latest_version_code,
				'url' 				=> $Version->url,
				'releaseNotes' 		=> $Version->release_notes
			]);
		} catch (\Exception $e) {
			return $this->sendException($e, 'latestVersion');
		}
	}

	public function merchantStore(Request $request)
	{
		try {
			$Merchant = new Merchant;
			$Merchant->username = $request->username;
			$Merchant->password = md5($request->password);
			$Merchant->nama_merchant = $request->nama_merchant;
			$Merchant->no_telp = $request->no_telp;
			$Merchant->address = $request->address;
			$Merchant->is_active = 'n';
			$Merchant->email = $request->email;
			$Merchant->save();

			return $this->sendResponse('Merchant stored successfully', $Merchant);
		} catch (\Exception $e) {
			return $this->sendException($e, 'merchantStore');
		}
	}

	public function loginMitra(Request $request)
	{
		try {
			if ($request->login_as == 'merchant') {
				$Merchant = Merchant::where('username', $request->username)
				->where('password', md5($request->password))
				->first();

				if ($Merchant != null) {
					$Merchant->setAttribute('login_as', $request->login_as);
					return $this->sendResponse('Login success', new LoginMitraResource($Merchant));
				}
			}else if($request->login_as == 'courier'){
				$Courier = Courier::where('username', $request->username)
				->where('password', md5($request->password))
				->first();

				if ($Courier != null) {
					$Courier->setAttribute('login_as', $request->login_as);
					return $this->sendResponse('Login success', new LoginMitraResource($Courier));
				}
			}

			return $this->sendError('Login failed, please check your username or password');
		} catch (\Exception $e) {
			return $this->sendException($e, 'loginMitra');
		}
    }

    public function registerMitra(Request $request)
    {
        try {
            $request->validate([
                'register_as' => 'required',
                'name' => 'required',
                'email' => 'required',
                'username' => 'required',
                'password' => 'required',
                'phone' => 'required',
            ]);

            if ($request->register_as == 'merchant') {
                $request->validate([
                    'address' => 'required',
                ]);

                //Check if username already exist
                $merchantCount = Merchant::where('username', $request->username)->count();
                if ($merchantCount > 0) {
                    return $this->sendError('User already exist');
                }

                $newMerchant = new Merchant;
                $newMerchant->username = $request->username;
                $newMerchant->password = $request->password;
                $newMerchant->nama_merchant = $request->name;
                $newMerchant->no_telp = $request->phone;
                $newMerchant->address = $request->address;
                $newMerchant->is_active = 'n';
                $newMerchant->email = $request->email;
                $newMerchant->mdr = '1';
                $newMerchant->description = NULL;
                $newMerchant->image = NULL;

                if ($newMerchant->save()) {
                    return $this->sendResponse('User registered successfully', new MerchantResource($newMerchant));
                } else {
                    return $this->sendError('User registration failed');
                }
            } else if ($request->register_as == 'courier') {
                $request->validate([
                    'id_merchant' => 'required',
                ]);

                //Check if username already exist
                $courierCount = Courier::where('username', $request->username)->count();
                if ($courierCount > 0) {
                    return $this->sendError('User already exist');
                }

                $newCourier = new Courier;
                $newCourier->id_merchant = $request->id_merchant;
                $newCourier->username = $request->username;
                $newCourier->password = $request->username;
                $newCourier->nama = $request->name;
                $newCourier->nohp = $request->phone;
                $newCourier->email = $request->email;
                $newCourier->is_active = 'n';
                $newCourier->lat = NULL;
                $newCourier->lon = NULL;
                $newCourier->radius = NULL;

                if ($newCourier->save()) {
                    return $this->sendResponse('User registered successfully', new CourierResource($newCourier));
                } else {
                    return $this->sendError('User registration failed');
                }
            }

            return $this->sendError('Request format invalid');

        } catch (\Exception $e) {
            return $this->sendException($e, 'registerMitra');
        }
    }

	public function brandList()
	{
		try {
			$Brands = Brand::orderBy('nama_parent', 'asc')->get();

			return $this->sendResponse('Brand retrieved successfully', BrandResource::collection($Brands));
		} catch (\Exception $e) {
			return $this->sendException($e, 'brandList');
		}
	}

	public function brandStore(Request $request)
	{
		try {
			$Brand = new Brand;
			$Brand->nama_parent = $request->nama_parent;
			$Brand->id_kategori = $request->id_kategori;
			$Brand->id_merchant = $request->id_merchant;
			$Brand->save();

			return $this->sendResponse('Brand stored successfully', new BrandResource($Brand));
		} catch (\Exception $e) {
			return $this->sendException($e, 'brandStore');
		}
	}

	public function categoryList()
	{
		try {
			$Kategoris = Kategori::orderBy('nama_kategori', 'asc')->get();

			return $this->sendResponse('Category stored successfully', KategoriResource::collection($Kategoris));
		} catch (\Exception $e) {
			return $this->sendException($e, 'categoryList');
		}
	}

	public function categoryStore(Request $request)
	{
		try {
			$Kategori = new Kategori;
			$Kategori->nama_kategori = $request->nama_kategori;
			$Kategori->kode_kategori = $request->kode_kategori;
			$Kategori->save();

			return $this->sendResponse('Category stored successfully', new KategoriResource($Kategori));
		} catch (\Exception $e) {
			return $this->sendException($e, 'categoryAdd');
		}
	}

	public function stokStore(Request $request)
	{
		try {
			$Stok = new Stok;
			$Stok->id_produk = $request->id_produk;
			$Stok->jumlah_stok = $request->jumlah_stok;
			$Stok->waktu = Carbon::now;
			$Stok->ket = null;
			$Stok->save();

			return $this->sendResponse('Product stock stored successfully', $Stok);
		} catch (\Exception $e) {
			return $this->sendException($e, 'stokStore');
		}
	}

	public function confirmationListMerchant($id_merchant)
	{
		try {
			$TransaksiDetails = TransaksiDetail::leftJoin('produk', 'transaksidetail.id_produk', '=', 'produk.id')
			->where('produk.id_merchant', $id_merchant)
			->whereNotIn('machine', ['machine001'])
			->get();

			$transaksiIds = [];
			foreach ($TransaksiDetails as $TransaksiDetail) {
				array_push($transaksiIds, $TransaksiDetail->id_transaksi);
			}

			$Transaksis = Transaksi::whereIn('id', $transaksiIds)->where('status', '0')->get();

			return $this->sendResponse('Confirmation list retrieved successfully', TransaksiResource::collection($Transaksis));
		} catch (\Exception $e) {
			return $this->sendException($e, 'confirmationListMerchant');
		}
	}

	public function courierListByMerchant($id_merchant)
	{
		try {
			$Couriers = Courier::where('id_merchant', $id_merchant)->get();
			$courierDefault = Courier::find(1);

			$Couriers->push($courierDefault);

			return $this->sendResponse('Courier retrieved successfully', CourierResource::collection($Couriers));
		} catch (\Exception $e) {
			return $this->sendException($e, 'courierListByMerchant');
		}
	}

	public function merchantConfirmTransaction(Request $request)
	{
		try {
			$Transaksi = Transaksi::find($request->id_transaction);
			if ($request->id_courier == '0') {
				$Transaksi->status = '3';
			} else {
				$Transaksi->status = '1';
			}
			$Transaksi->id_courier = $request->id_courier;

			if ($Transaksi->save()) {
				$User = User::find($Transaksi->machine);
				$noHpBuyer = $User->phone;

				//release payment
				if ($Transaksi->status == '3') {
					$url = $this->getVarMypayApiUrl() . 'payment/mystore/release';
					$params = [
						'transaction_number' 	=> $Transaksi->no_ref
					];
					$headers = [
						'Accept: application/json',
						'X-Requested-With: XMLHttpRequest',
						'Content-Type: multipart/form-data'
					];

					$response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
					if ($response['status'] != 200) {
						$Transaksi = Transaksi::find($request->id_transaction);
						$Transaksi->status = '0';
						$Transaksi->save();
						return $this->sendError('Transaction failed to confirm (code: 0x001)');
					}

					//send notification customer
					if ($response['status'] == 200) {
						$title = 'Status Transaksi #' . $Transaksi->no_ref;
						$message = 'Transaksi anda telah dikonfirmasi';
						$idTransaction = $Transaksi->id;
						$topics = $this->prefix_notif_user.$noHpBuyer;
						FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);

						return $this->sendResponse('Transaction confirmed successfully', new TransaksiResource($Transaksi));
					}
					//send notification customer
				}
				//release payment

				//send notification courier
				$Courier = Courier::find($request->id_courier);
				$title = 'Permintaan Pengiriman #' . $Transaksi->no_ref;
				$inner_title = 'Permintaan pengiriman menunggu konfirmasi';
				$message = '';
				$topics = $this->prefix_notif_courier.$Courier->nohp;
				FirebaseNotification::send($title, $inner_title, $message, $topics);
				//send notification courier

				//send notification customer
				$title = 'Status Transaksi #' . $Transaksi->no_ref;
				$message = 'Transaksi anda telah dikonfirmasi dan akan dikirim oleh kurir';
				$idTransaction = $Transaksi->id;
				$topics = $this->prefix_notif_user.$noHpBuyer;
				FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
				//send notification customer

				return $this->sendResponse('Transaction confirmed successfully', new TransaksiResource($Transaksi));
			}

			return $this->sendError('Transaction failed to confirm (code: 0x002)');
		} catch (\Exception $e) {
			return $this->sendException($e, 'merchantConfirmTransaction');
		}
	}

	public function merchantHistory($id_merchant)
	{
		try {
			$TransaksiDetails = TransaksiDetail::leftJoin('produk', 'transaksidetail.id_produk', '=', 'produk.id')
			->where('produk.id_merchant', $id_merchant)
			->whereNotIn('machine', ['machine001'])
			->get();

			$transaksiIds = [];
			foreach ($TransaksiDetails as $TransaksiDetail) {
				array_push($transaksiIds, $TransaksiDetail->id_transaksi);
			}

			$Transaksis = Transaksi::whereIn('id', $transaksiIds)->whereIn('status', ['1','2','3'])->orderBy('waktu', 'desc')->get();

			return $this->sendResponse('Confirmation list retrieved successfully', TransaksiResource::collection($Transaksis));
		} catch (\Exception $e) {
			return $this->sendException($e, 'merchantHistory');
		}
	}

	public function confirmationListCourier($id_courier)
	{
		try {
			$Transaksis = Transaksi::where('id_courier', $id_courier)
			->whereIn('status', ['1', '2'])
			->whereNotIn('machine', ['machine001'])
			->get();

			return $this->sendResponse('Confirmation list retrieved successfully', TransaksiResource::collection($Transaksis));
		} catch (\Exception $e) {
			return $this->sendException($e, 'confirmationListCourier');
		}
	}

	public function courierConfirmTransaction(Request $request)
	{
		try {
			$Transaksi = Transaksi::find($request->id_transaction);
			if ($Transaksi->status < 3) {
				$Transaksi->status = $Transaksi->status + 1;
			}else{
				$Transaksi->status = $Transaksi->status;
			}
			$TransactionStatusBefore = $Transaksi->status;

			if ($Transaksi->save()) {
				$User = User::find($Transaksi->machine);
				$noHpBuyer = $User->phone;

				//release payment
				if ($Transaksi->status == '3') {
					$url = $this->getVarMypayApiUrl() . 'payment/mystore/release';
					$params = [
						'transaction_number' 	=> $Transaksi->no_ref
					];
					$headers = [
						'Accept: application/json',
						'X-Requested-With: XMLHttpRequest',
						'Content-Type: multipart/form-data'
					];

					$response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
					if ($response['status'] != 200) {
						$Transaksi = Transaksi::find($request->id_transaction);
						$Transaksi->status = $TransactionStatusBefore;
						$Transaksi->save();
						return $this->sendError('Transaction failed to confirm (code: 0x001)');
					}
				}
				//release payment

				//send notification customer
				$title = 'Status Transaksi #' . $Transaksi->no_ref;
				if ($Transaksi->status == 2) {
					$message = 'Pesanan anda sedang dikirim oleh kurir';
				}else if($Transaksi->status == 3){
					$message = 'Pesanan telah diterima';
				}
				$idTransaction = $Transaksi->id;
				$topics = $this->prefix_notif_user.$noHpBuyer;
				FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
				//send notification customer

				return $this->sendResponse('Transaction confirmed successfully', new TransaksiResource($Transaksi));
			}

			return $this->sendError('Transaction failed to confirm (code: 0x002)');
		} catch (\Exception $e) {
			return $this->sendException($e, 'courierConfirmTransaction');
		}
	}

	public function courierHistory($id_courier)
	{
		try {
			$Transaksis = Transaksi::where('id_courier', $id_courier)
			->where('status', '3')
			->whereNotIn('machine', ['machine001'])
			->get();

			return $this->sendResponse('Confirmation list retrieved successfully', TransaksiResource::collection($Transaksis));
		} catch (\Exception $e) {
			return $this->sendException($e, 'courierHistory');
		}
    }
	//END MITRA SECTION

	//START IMAGE SECTION
	public function imageOriginal($image_code)
	{
		$image_code = substr($image_code, 3);
		$image_code = substr($image_code, 0, -2);
		$img = ProdukImage::find($image_code)->original;
		$path = public_path('product_image') . '/' . $img;

		if (File::exists($path)) {
			return Image::make($path)->response('png');
		}else{
			return "Image Not Found";
		}
	}

	public function imageThumbnail($image_code)
	{
		$image_code = substr($image_code, 3);
        $image_code = substr($image_code, 0, -2);
        $img = '';
        $findProdukImage = ProdukImage::find($image_code);
        if ($findProdukImage != null) {
            $img = $findProdukImage->original;
        }
		$path = public_path('product_image') . '/' . $img;

		if (File::exists($path) && $img != '') {
			return Image::make($path)->resize(null, 300, function($constraint)
			{
				$constraint->aspectRatio();
				$constraint->upsize();
			})->response('jpg');
		}else{
			return "Image Not Found";
		}
	}

	public function imageBanner($image_code)
	{

		$image_code = substr($image_code, 3);
		$image_code = substr($image_code, 0, -2);
        $img = '';
        $findBanner = Banner::find($image_code);
        if ($findBanner != null) {
            $img = $findBanner->image;
        }
		$path = public_path('banner_image') . '/' .$img;

		if (File::exists($path) && $img != '') {
			return Image::make($path)->resize(600, 300)->response('png');
		}else{
			return "Image Not Found";
		}
	}

	public function imageMerchant($merchant_id)
	{
		$merchant_id = substr($merchant_id, 3);
        $merchant_id = substr($merchant_id, 0, -2);
        $img = '';
        $findMerchant = Merchant::find($merchant_id);
        if ($findMerchant != null) {
            $img = $findMerchant->image;
        }
        $path = public_path('merchant_image') . '/' . $img;

		if (File::exists($path) && $img != '') {
			return Image::make($path)->resize(600, null, function($constraint)
			{
				$constraint->aspectRatio();
				$constraint->upsize();
			})->response('jpg');
		}else{
            $path = public_path('merchant_image') . '/default.png';
            return Image::make($path)->resize(600, null, function($constraint)
			{
				$constraint->aspectRatio();
				$constraint->upsize();
			})->response('jpg');
		}
	}
	//END IMAGE SECTION

	//START BRIDGE SECTION
	public function bridgeMerchant($merchant_number)
	{
		$Merchant = Merchant::where('no_telp', $merchant_number)->first();

		if ($Merchant != null) {
			return response()->json([
				'success'   => true,
				'message'   => 'Merchant retrieved successfully',
				'data'      => [
					'nama_merchant' => $Merchant->nama_merchant
				]
			], 200);
		}else{
			return response()->json([
				'success'   => false,
				'message'   => 'Merchant not found',
				'data'      => [
					'nama_merchant' => null
				]
			], 200);
		}
	}

	public function bridgePayment(Request $request)
	{
		try {
			$nominal = $request->nominal;
			$machine = $request->machine;
			$Merchant = Merchant::where('no_telp', $request->merchant_number)->first();
			$no_ref  = TrxCode::generate($Merchant->id);

			$TextMessage = new TextMessage;
			$TextMessage->updatetime = Carbon::now();
			$TextMessage->sender = 'MYPAY';
			$TextMessage->time = Carbon::now();
			$TextMessage->text = 'MYPAY|'.$nominal.'|'.$machine.'|'.$no_ref;

			if ($TextMessage->save()) {
				return $this->sendResponse('Transaction code stored successfully', '');
			}else{
				return $this->sendError('Transaction code failed to store');
			}

		} catch (\Exception $e) {
			return $this->sendException($e, 'paymentSuccess');
		}
	}

	public function bridgeChangeNoHP(Request $request)
	{
		try {
			$Merchant = Merchant::where('no_telp', $request->phone_before)->first();
			$Merchant->no_telp = $request->phone_after;
			$Merchant->save();

			return $this->sendResponse('Number updated successfully', '');
		} catch (\Exception $e) {
			return $this->sendException($e, 'bridgeChangeNoHP');
		}
	}

	public function bridgeVarPercentage()
	{
		try {
			$percentage = Variable::where('param', 'persentase')->first()->value;
			return $this->sendResponse('Percentage retrieved successfully', ['percentage' => $percentage]);
		} catch (\Exception $e) {
			return $this->sendException($e, 'bridgeVarPercentage');
		}
	}
	//END BRIDGE SECTION

	public function bannerList()
	{
		try {
			$Banners = Banner::all();

			return $this->sendResponse('Banner retrieved successfully', BannerResource::collection($Banners));
		} catch (\Exception $e) {
			return $this->sendException($e, 'bannerList');
		}
	}

	public function courierConfirmAll(Request $request)
	{
		try {
			$status = $request->status - 1;
			$Transaksis = Transaksi::where('id_courier', $request->id_courier)->where('status', $status)->get();

			foreach ($Transaksis as $Transaksi) {
				$currentTrans = Transaksi::find($Transaksi->id);
				$currentTrans->status = $request->status;

				if ($currentTrans->save()) {
					//send notification customer
					$User = User::where('id', $currentTrans->machine)->first();
					$noHpBuyer = $User->nohp;

					$title = 'Status Transaksi #' . $currentTrans->no_ref;
					if ($currentTrans->status == 2) {
						$message = 'Pesanan anda sedang dikirim oleh kurir';
					}else if($currentTrans->status == 3){
						$message = 'Pesanan telah diterima';
					}
					$idTransaction = $currentTrans->id;
					$topics = $this->prefix_notif_user.$noHpBuyer;
					FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
					//send notification customer
				}

			}

			return $this->sendResponse('Transaction confirmed successfully', '');
		} catch (\Exception $e) {
			return $this->sendException($e, 'courierConfirmAll');
		}
	}

	public function generateQrcodeMypay($qrcode)
	{
		try {
			return Image::make(QrCode::format('png')->size(512)->margin(1)->merge(public_path('assets_image/qrcode_mypay_logo.png'), .3, true)->errorCorrection('H')->generate($qrcode))->response();
		} catch (\Exception $e) {
			return $this->sendException($e, 'generateQrcode');
		}
	}

	public function generateQrcodeLinkaja($qrcode)
	{
		try {
			return Image::make(QrCode::format('png')->size(512)->margin(1)->merge(public_path('assets_image/qrcode_linkaja_logo.png'), .3, true)->errorCorrection('H')->generate($qrcode))->response();
		} catch (\Exception $e) {
			return $this->sendException($e, 'generateQrcode');
		}
    }

    public function userStatus(Request $request)
    {
        try {
            $merchant = Merchant::where('no_telp', $request->phone)->first();
            $courier = Courier::where('nohp', $request->phone)->first();

            return $this->sendResponse('User status retrieved successfully', [
                'merchant'  => isset($merchant) ? true : false,
                'courier'   => isset($courier) ? true : false
            ]);
        } catch (\Exception $e) {
			return $this->sendException($e, 'userStatus');
		}
    }

}
