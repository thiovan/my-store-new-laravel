<?php

namespace App\Http\Controllers\api\v2;

use Carbon\Carbon;
use App\Http\Functions\Logs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PlaygroundController extends Controller
{
    public function index()
    {
        // $files = Storage::files('test_logs/2019-09-20');
        // dd(Carbon::now()->timestamp);
        $logs = new Logs('test');
        $logs2 = new Logs('test');
        $logs2->generate('test log2 1');
        $logs->generate('test log1 1');
        $logs->generate('test log1 2');
        $logs2->generate('test log2 2');
    }
}
