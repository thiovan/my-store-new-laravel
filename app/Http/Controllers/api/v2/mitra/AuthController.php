<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\Courier;
use App\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Functions\CurlRequest;
use App\Http\Resources\CourierResource;
use App\Http\Controllers\BaseController;
use App\Http\Resources\MerchantResource;
use App\Http\Resources\LoginMitraResource;

class AuthController extends BaseController
{
    public function loginMypay($username, $password)
    {
        try {
            $url = $this->getVarMypayApiUrl() . 'auth/login';

            $params = [
                'grant_type' => 'password',
                'client_id' => $this->getVarClientId(),
                'client_secret' => $this->getVarClientSecret(),
                'username' => $username,
                'password' => $password,
                'scope' => '*',
            ];
            $headers = [
                'Accept: application/json',
                'X-Requested-With: XMLHttpRequest',
                'Content-Type: multipart/form-data'
            ];
            $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);

            return $response;
        } catch (\Exception $e) {
            return $this->sendException($e, 'AuthController/loginMypay');
        }
    }

    public function validateUser(Request $request)
    {
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required',
            ]);

            // check if phone already exist
            $merchantCount = Merchant::where('no_telp', $request->username)->count();
            if ($merchantCount > 0) {
                return $this->sendError('Phone number already registered');
            }

            $loginMypay = $this->loginMypay($request->username, $request->password);
            if ($loginMypay['status'] != 200) {
                return $this->sendError('Validation failed, please check your username or password');
            }
            if ($loginMypay['response']->is_verified == 0) {
                return $this->sendError('Validation failed, your mypay account not verified');
            }

            return $this->sendResponse('Validation success', $loginMypay['response']);
        } catch (\Exception $e) {
            return $this->sendException($e, 'AuthController/validateUser');
        }
    }

    public function login(Request $request)
    {
        try {
            $request->validate([
                'phone' => 'required',
                'password' => 'required',
                'login_as' => 'required',
            ]);

            if ($request->login_as == 'merchant') {
                $loginMypay = $this->loginMypay($request->phone, $request->password);
                if ($loginMypay['status'] != 200) {
                    if (isset($loginMypay['response']->message)) {
                        return $this->sendError('MYPAY AUTH FAILED: ' . $loginMypay['response']->message);
                    }
                    return $this->sendError('MYPAY AUTH FAILED: please check your username or password');
                }

                $Merchant = Merchant::where('no_telp', $request->phone)->first();

                if ($Merchant != null) {
                    if ($Merchant->is_active == 'n') {
                        return $this->sendError('MYSTORE AUTH FAILED: account waiting approval by admin');
                    }

                    if (md5($request->password) != $Merchant->password) {
                        $Merchant->password = md5($request->password);
                        $Merchant->save();
                    }

                    $Merchant->setAttribute('login_as', $request->login_as);
                    $Merchant->setAttribute('token', 'Bearer ' . $loginMypay['response']->access_token);
                    return $this->sendResponse('Login success', new LoginMitraResource($Merchant));
                } else {
                    return $this->sendError('MYSTORE AUTH FAILED: merchant not found');
                }
            } else {
                $Courier = Courier::where('phone', $request->phone)
                    ->where('password', md5($request->password))
                    ->first();

                if ($Courier != null) {
                    if ($Courier->is_active == 'n') {
                        return $this->sendError('MYSTORE AUTH FAILED: account not activated by merchant');
                    }

                    $Courier->setAttribute('login_as', $request->login_as);
                    $Courier->setAttribute('token', '');
                    return $this->sendResponse('Login success', new LoginMitraResource($Courier));
                } else {
                    return $this->sendError('MYSTORE AUTH FAILED: please check your username or password');
                }
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'AuthController/login');
        }
    }

    public function register(Request $request)
    {
        try {
            $request->validate([
                'register_as' => 'required',
                'phone' => 'required',
                'password' => 'required',
            ]);

            // filter phone number
            $request->phone = preg_replace('/[^0-9]/', '', $request->phone);
            if (substr($request->phone, 0, 2) == '62') {
                $request->phone = '0' . substr($request->phone, 2, strlen($request->phone));
            }

            if ($request->register_as == 'merchant') {
                $request->validate([
                    'name'          => 'required',
                    'address'       => 'required',
                    'description'   => 'required',
                    'latitude'      => 'required',
                    'longitude'     => 'required',
                ]);

                // check if user registered to mypay
                $loginMypay = $this->loginMypay($request->phone, $request->password);
                if ($loginMypay['status'] != 200) {
                    return $this->sendError('Validation failed, please check your username or password');
                }

                // check if user verified
                if ($loginMypay['response']->is_verified == 0) {
                    return $this->sendError('Validation failed, your mypay account not verified');
                }

                // check if phone already exist
                $merchantCount = Merchant::where('no_telp', $request->phone)->count();
                if ($merchantCount > 0) {
                    return $this->sendError('Phone number already registered');
                }

                DB::beginTransaction();
                try {
                    $newMerchant = new Merchant;
                    $newMerchant->username = $request->phone;
                    $newMerchant->password = md5($request->password);
                    $newMerchant->nama_merchant = $request->name;
                    $newMerchant->no_telp = $request->phone;
                    $newMerchant->address = $request->address;
                    $newMerchant->is_active = 'n';
                    $newMerchant->email = $loginMypay['response']->email;
                    $newMerchant->mdr = '1';
                    $newMerchant->description = $request->description;
                    $newMerchant->image = NULL;
                    $newMerchant->lat = $request->latitude;
                    $newMerchant->lon = $request->longitude;
                    $newMerchant->save();

                    $newCourier = new Courier;
                    $newCourier->id_merchant = $newMerchant->id;
                    $newCourier->username = $request->phone;
                    $newCourier->password = md5($request->password);
                    $newCourier->nama = 'Kurir ' . $request->name;
                    $newCourier->nohp = $request->phone;
                    $newCourier->email = $loginMypay['response']->email;
                    $newCourier->is_active = 'y';
                    $newCourier->lat = NULL;
                    $newCourier->lon = NULL;
                    $newCourier->radius = NULL;
                    $newCourier->save();
                    DB::commit();
                    return $this->sendResponse('Merchant registered successfully', new MerchantResource($newMerchant));
                } catch (\PDOException $e) {
                    DB::rollBack();
                    return $this->sendError('Merchant registration failed');
                }
            } else if ($request->register_as == 'courier') {
                $request->validate([
                    'id_merchant'   => 'required',
                    'name'          => 'required',
                    'email'         => 'required',
                ]);

                // check if phone already exist
                $courierCount = Courier::where('nohp', $request->phone)->count();
                if ($courierCount > 0) {
                    return $this->sendError('Phone number already registered');
                }

                $newCourier = new Courier;
                $newCourier->id_merchant = $request->id_merchant;
                $newCourier->username = $request->phone;
                $newCourier->password = md5($request->password);
                $newCourier->nama = $request->name;
                $newCourier->nohp = $request->phone;
                $newCourier->email = $request->email;
                $newCourier->is_active = 'y';
                $newCourier->lat = NULL;
                $newCourier->lon = NULL;
                $newCourier->radius = NULL;

                if ($newCourier->save()) {
                    return $this->sendResponse('User registered successfully', new CourierResource($newCourier));
                } else {
                    return $this->sendError('User registration failed');
                }
            }

            return $this->sendError('Request format invalid');
        } catch (\Exception $e) {
            return $this->sendException($e, 'AuthController/register');
        }
    }

    public function changePassword(Request $request, $id_user)
    {
        try {
            $request->validate([
                'old_password' => 'required',
                'new_password' => 'required',
                'privilege'    => 'required'
            ]);

            if ($request->privilege == 'merchant') {

                if (!$request->headers->has('Authorization')) {
                    return $this->sendError('Authorization failed');
                }

                $url = $this->getVarMypayApiUrl() . 'auth/update/password';
                $params = [
                    'oldPassword'           => $request->old_password,
                    'password'              => $request->new_password,
                    'password_confirmation' => $request->new_password,
                ];
                $headers = [
                    'Accept: application/json',
                    'X-Requested-With: XMLHttpRequest',
                    'Content-Type: application/x-www-form-urlencoded',
                    'Authorization: ' . $request->header('Authorization')
                ];

                $findMerchant = Merchant::find($id_user);
                if ($findMerchant != null) {

                    if ($findMerchant->password == md5($request->old_password)) {

                        $findMerchant->password = md5($request->new_password);
                        if (!$findMerchant->save()) {
                            return $this->sendError('Change password failed (code: 0x001)');
                        }

                        $response = CurlRequest::putWithHeader($url, $params, $headers);
                        if ($response['status'] != 200) {
                            $findMerchant->password = md5($request->old_password);
                            $findMerchant->save();

                            return $this->sendError(isset($response['response']->message) ? $response['response']->message : 'Change password failed (code: 0x002)');
                        }

                        return $this->sendResponse('Password updated', new MerchantResource($findMerchant));
                    } else {
                        return $this->sendError('Your current password does not matches with the password you provided. Please try again.');
                    }
                } else {
                    return $this->sendError('User not found');
                }
            }

            if ($request->privilege == 'courier') {

                $findCourier = Courier::find($id_user);
                if ($findCourier != null) {

                    if ($findCourier->password == md5($request->old_password)) {

                        $findCourier->password = md5($request->new_password);
                        $findCourier->save();

                        return $this->sendResponse('Password updated', new CourierResource($findCourier));
                    } else {
                        return $this->sendError('Wrong password');
                    }
                } else {
                    return $this->sendError('User not found');
                }
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'AuthController/changePassword');
        }
    }
}
