<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Http\Controllers\BaseController;

class BrandController extends BaseController
{
    public function brandByMerchant($id_merchant)
    {
        try {
            $Brands = Brand::where('id_merchant', $id_merchant)->orderBy('nama_parent', 'asc')->get();

            return $this->sendResponse('Brand retrieved successfully', BrandResource::collection($Brands));
        } catch (\Exception $e) {
            return $this->sendException($e, 'BrandController/index');
        }
    }

    public function store(Request $request)
    {
        try {
            $sameBrand = Brand::where('nama_parent', $request->nama_parent)
            ->where('id_kategori', $request->id_kategori)
            ->where('id_merchant', $request->id_merchant)
            ->count();
            if ($sameBrand > 0) {
                return $this->sendError('Brand already exist');
            }

            $Brand = new Brand;
            $Brand->nama_parent = $request->nama_parent;
            $Brand->id_kategori = $request->id_kategori;
            $Brand->id_merchant = $request->id_merchant;
            $Brand->save();

            return $this->sendResponse('Brand stored successfully', new BrandResource($Brand));
        } catch (\Exception $e) {
            return $this->sendException($e, 'BrandController/store');
        }
    }
}
