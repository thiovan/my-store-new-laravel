<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\User;
use App\Courier;
use App\Transaksi;
use Illuminate\Http\Request;
use App\Http\Functions\CurlRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourierResource;
use App\Http\Controllers\BaseController;
use App\Http\Resources\TransaksiResource;
use App\Http\Functions\FirebaseNotification;

class CourierController extends BaseController
{
    public function courierConfirmList($id_courier)
    {
        try {
            $Transaksis = Transaksi::where('id_courier', $id_courier)
                ->whereIn('status', ['1', '2'])
                ->whereNotIn('machine', ['machine001'])
                ->get();

            return $this->sendResponse('Confirmation list retrieved successfully', TransaksiResource::collection($Transaksis));
        } catch (\Exception $e) {
            return $this->sendException($e, 'CourierController/courierConfirmList');
        }
    }

    public function courierConfirm(Request $request)
    {
        try {
            $Transaksi = Transaksi::find($request->id_transaction);
            if ($Transaksi->status < 3) {
                $Transaksi->status = $Transaksi->status + 1;
            } else {
                $Transaksi->status = $Transaksi->status;
            }
            $TransactionStatusBefore = $Transaksi->status;

            if ($Transaksi->save()) {
                $User = User::find($Transaksi->machine);
                $noHpBuyer = $User->phone;

                //release payment
                if ($Transaksi->status == '3') {
                    $url = $this->getVarMypayApiUrl() . 'payment/mystore/release';
                    $params = [
                        'transaction_number'     => $Transaksi->no_ref
                    ];
                    $headers = [
                        'Accept: application/json',
                        'X-Requested-With: XMLHttpRequest',
                        'Content-Type: multipart/form-data'
                    ];

                    $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
                    if ($response['status'] != 200) {
                        $Transaksi = Transaksi::find($request->id_transaction);
                        $Transaksi->status = '2';
                        $Transaksi->save();
                        return $this->sendError('Transaction failed to confirm (code: 0x001)');
                    }
                }
                //release payment

                //send notification customer
                $title = 'Status Transaksi #' . $Transaksi->no_ref;
                if ($Transaksi->status == 2) {
                    $message = 'Pesanan anda sedang dikirim oleh kurir';
                } else if ($Transaksi->status == 3) {
                    $message = 'Pesanan telah diterima';
                }
                $idTransaction = $Transaksi->id;
                $topics = $this->getVarPrefixNotif()['user'] . $noHpBuyer;
                FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                //send notification customer

                return $this->sendResponse('Transaction confirmed successfully', new TransaksiResource($Transaksi));
            }

            return $this->sendError('Transaction failed to confirm (code: 0x002)');
        } catch (\Exception $e) {
            return $this->sendException($e, 'CourierController/courierConfirm');
        }
    }

    public function courierhistory($id_courier)
    {
        try {
            $Transaksis = Transaksi::where('id_courier', $id_courier)
                ->where('status', '3')
                ->whereNotIn('machine', ['machine001'])
                ->get();

            return $this->sendResponse('Courier history retrieved successfully', TransaksiResource::collection($Transaksis));
        } catch (\Exception $e) {
            return $this->sendException($e, 'CourierController/courierHistory');
        }
    }

    public function courierConfirmAll(Request $request)
    {
        try {
            $status = $request->status - 1;
            $Transaksis = Transaksi::where('id_courier', $request->id_courier)->where('status', $status)->get();

            foreach ($Transaksis as $Transaksi) {
                $currentTrans = Transaksi::find($Transaksi->id);
                $TransactionStatusBefore = $currentTrans->status;
                $currentTrans->status = $request->status;

                if ($currentTrans->save()) {
                    //send notification customer
                    $User = User::where('id', $currentTrans->machine)->first();
                    $noHpBuyer = $User->nohp;

                    //release payment
                    if ($currentTrans->status == '3') {
                        $url = $this->getVarMypayApiUrl() . 'payment/mystore/release';
                        $params = [
                            'transaction_number'     => $currentTrans->no_ref
                        ];
                        $headers = [
                            'Accept: application/json',
                            'X-Requested-With: XMLHttpRequest',
                            'Content-Type: multipart/form-data'
                        ];

                        $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
                        if ($response['status'] != 200) {
                            $currentTrans = Transaksi::find($Transaksi->id);
                            $currentTrans->status = '2';
                            $currentTrans->save();
                            return $this->sendError('Transaction failed to confirm (code: 0x001)');
                        }
                    }
                    //release payment

                    $title = 'Status Transaksi #' . $currentTrans->no_ref;
                    if ($currentTrans->status == 2) {
                        $message = 'Pesanan anda sedang dikirim oleh kurir';
                    } else if ($currentTrans->status == 3) {
                        $message = 'Pesanan telah diterima';
                    }
                    $idTransaction = $currentTrans->id;
                    $topics = $this->getVarPrefixNotif()['user'] . $noHpBuyer;
                    FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                    //send notification customer
                }
            }

            return $this->sendResponse('Transaction confirmed successfully', '');
        } catch (\Exception $e) {
            return $this->sendException($e, 'CourierController/courierConfirmAll');
        }
    }

    public function courierByMerchant($id_merchant)
    {
        try {
            $Couriers = Courier::where('id_merchant', $id_merchant)->get();
            $courierDefault = Courier::find(1);

            // $Couriers->push($courierDefault);

            return $this->sendResponse('Courier retrieved successfully', CourierResource::collection($Couriers));
        } catch (\Exception $e) {
            return $this->sendException($e, 'CourierController/courierByMerchant');
        }
    }

    public function changeCourierActive($id_courier, Request $request)
    {
        try {
            $request->validate([
                'is_active' => 'required',
            ]);

            $findCourier = Courier::find($id_courier);

            if ($findCourier == null) {
                return $this->sendError('Courier not found');
            }

            $findCourier->is_active = $request->is_active;

            if (!$findCourier->save()) {
                return $this->sendError('Failed update courier');
            }

            return $this->sendResponse('Courier updated successfully', new CourierResource($findCourier));
        } catch (\Exception $e) {
            return $this->sendException($e, 'CourierController/changeCourierActive');
        }
    }
}
