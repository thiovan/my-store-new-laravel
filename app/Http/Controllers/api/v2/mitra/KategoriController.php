<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Http\Resources\KategoriResource;

class KategoriController extends BaseController
{
    public function index()
    {
        try {
            $Kategoris = Kategori::orderBy('nama_kategori', 'asc')->get();

            return $this->sendResponse('Category retrieved successfully', KategoriResource::collection($Kategoris));
        } catch (\Exception $e) {
            return $this->sendException($e, 'KategoriController/index');
        }
    }

    public function store(Request $request)
    {
        try {
            $sameKategori = Kategori::where('kode_kategori', $request->kode_kategori)->count();
            if ($sameKategori > 0) {
                return $this->sendError('Category already exist');
            }

            $Kategori = new Kategori;
            $Kategori->nama_kategori = $request->nama_kategori;
            $Kategori->kode_kategori = $request->kode_kategori;
            $Kategori->save();

            return $this->sendResponse('Category stored successfully', new KategoriResource($Kategori));
        } catch (\Exception $e) {
            return $this->sendException($e, 'KategoriController/store');
        }
    }
}
