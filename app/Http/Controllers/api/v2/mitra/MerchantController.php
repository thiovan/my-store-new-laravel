<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\Stok;
use App\User;
use App\Produk;
use App\Courier;
use App\Kategori;
use App\Merchant;
use App\Transaksi;
use App\ProdukImage;
use App\TransaksiDetail;
use App\ProdukDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Functions\CurlRequest;
use App\Http\Functions\SkuGenerator;
use App\Http\Resources\ProdukResource;
use App\Http\Controllers\BaseController;
use App\Http\Resources\MerchantResource;
use App\Http\Resources\TransaksiResource;
use App\Http\Functions\FirebaseNotification;
use App\TransactionMypay;
use App\Http\Resources\TransactionMypayResource;

class MerchantController extends BaseController
{

    public function index()
    {
        try {
            $merchants = Merchant::all();
            return $this->sendResponse('Merchant retrieved successfully', MerchantResource::collection($merchants));
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/index');
        }
    }

    public function show($id_merchant)
    {
        try {
            $findMerchant = Merchant::find($id_merchant);

            if ($findMerchant != null) {
                return $this->sendResponse('Merchant retrieved successfully', new MerchantResource($findMerchant));
            }

            return $this->sendError('Merchant not found');
         } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/show');
        }
    }

    public function merchantPanel($id_merchant)
    {
        try {
            $produks = Produk::where('id_merchant', $id_merchant)->get();
            $stokProduk = 0;
            foreach ($produks as $produk) {
                $masuk = Stok::where('id_produk', $produk->id)->sum('jumlah_stok');
                $keluar = TransaksiDetail::where('id_produk', $produk->id)->sum('jumlah');
                $stokProduk += $masuk - $keluar;
            }

            $jumlahProduk = Produk::where('id_merchant', $id_merchant)->count();

            $totalPenjualan = TransaksiDetail::leftJoin('produk', 'transaksidetail.id_produk', '=', 'produk.id')
                ->where('id_merchant', $id_merchant)->sum('jumlah');

            $totalOmset = TransaksiDetail::leftJoin('produk', 'transaksidetail.id_produk', '=', 'produk.id')
                ->select(DB::raw('sum(transaksidetail.jumlah * transaksidetail.harga) AS jml'))
                ->where('id_merchant', $id_merchant)->first()->jml;

            $response = [];
            $response[0] = [
                'name'  => 'Stok Produk',
                'value' => (int)$stokProduk
            ];
            $response[1] = [
                'name'  => 'Jumlah Produk',
                'value' => (int)$jumlahProduk
            ];
            $response[2] = [
                'name'  => 'Total Penjualan',
                'value' => (int)$totalPenjualan
            ];
            $response[3] = [
                'name'  => 'Total Omset',
                'value' => (int)$totalOmset
            ];

            return $this->sendResponse('Merchant panel retrieved successfully', $response);
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/merchantPanel');
        }
    }

    public function merchantConfirmList($id_merchant)
    {
        try {
            $TransaksiDetails = TransaksiDetail::leftJoin('produk', 'transaksidetail.id_produk', '=', 'produk.id')
                ->where('produk.id_merchant', $id_merchant)
                ->whereNotIn('machine', ['machine001'])
                ->get();

            $transaksiIds = [];
            foreach ($TransaksiDetails as $TransaksiDetail) {
                array_push($transaksiIds, $TransaksiDetail->id_transaksi);
            }

            $Transaksis = Transaksi::whereIn('id', $transaksiIds)->where('status', '0')->get();

            return $this->sendResponse('Confirmation list retrieved successfully', TransaksiResource::collection($Transaksis));
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/merchantConfirmList');
        }
    }

    public function merchantConfirm(Request $request)
    {
        try {
            $Transaksi = Transaksi::find($request->id_transaction);
            if ($request->id_courier == '0') {
                $Transaksi->status = '3';
            } else {
                $Transaksi->status = '1';
            }
            $Transaksi->id_courier = $request->id_courier;

            if ($Transaksi->save()) {
                $User = User::find($Transaksi->machine);
                $noHpBuyer = $User->phone;

                //release payment
                if ($Transaksi->status == '3') {
                    $url = $this->getVarMypayApiUrl() . 'payment/mystore/release';
                    $params = [
                        'transaction_number'     => $Transaksi->no_ref
                    ];
                    $headers = [
                        'Accept: application/json',
                        'X-Requested-With: XMLHttpRequest',
                        'Content-Type: multipart/form-data'
                    ];

                    $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
                    if ($response['status'] != 200) {
                        $Transaksi = Transaksi::find($request->id_transaction);
                        $Transaksi->status = '0';
                        $Transaksi->save();
                        return $this->sendError('Transaction failed to confirm (code: 0x001)');
                    }

                    //send notification customer
                    if ($response['status'] == 200) {
                        $title = 'Status Transaksi #' . $Transaksi->no_ref;
                        $message = 'Transaksi anda telah dikonfirmasi';
                        $idTransaction = $Transaksi->id;
                        $topics = $this->getVarPrefixNotif()['user'] . $noHpBuyer;
                        FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);

                        return $this->sendResponse('Transaction confirmed successfully', new TransaksiResource($Transaksi));
                    }
                    //send notification customer
                }
                //release payment

                //send notification courier
                $Courier = Courier::find($request->id_courier);
                $title = 'Permintaan Pengiriman #' . $Transaksi->no_ref;
                $inner_title = 'Permintaan pengiriman menunggu konfirmasi';
                $message = '';
                $topics = $this->getVarPrefixNotif()['courier'] . $Courier->nohp;
                FirebaseNotification::send($title, $inner_title, $message, $topics);
                //send notification courier

                //send notification customer
                $title = 'Status Transaksi #' . $Transaksi->no_ref;
                $message = 'Transaksi anda telah dikonfirmasi dan akan dikirim oleh kurir';
                $idTransaction = $Transaksi->id;
                $topics = $this->getVarPrefixNotif()['user'] . $noHpBuyer;
                FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                //send notification customer

                return $this->sendResponse('Transaction confirmed successfully', new TransaksiResource($Transaksi));
            }

            return $this->sendError('Transaction failed to confirm (code: 0x002)');
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/merchantConfirm');
        }
    }

    public function merchantHistory($id_merchant)
    {
        try {
            $TransaksiDetails = TransaksiDetail::leftJoin('produk', 'transaksidetail.id_produk', '=', 'produk.id')
                ->where('produk.id_merchant', $id_merchant)
                ->whereNotIn('machine', ['machine001'])
                ->get();

            $transaksiIds = [];
            foreach ($TransaksiDetails as $TransaksiDetail) {
                array_push($transaksiIds, $TransaksiDetail->id_transaksi);
            }

            $Transaksis = Transaksi::whereIn('id', $transaksiIds)->whereIn('status', ['1', '2', '3'])->orderBy('waktu', 'desc')->get();

            return $this->sendResponse('Merchant history retrieved successfully', TransaksiResource::collection($Transaksis));
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/merchantHistory');
        }
    }

    public function confirmCancel(Request $request)
    {
        try {
            $Transaksi = Transaksi::find($request->id_transaction);
            $User = User::find($Transaksi->machine);

            if ($Transaksi->status != '0') {
                return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store (code: 0x001)');
            }

            if ($request->type == 'cancel') {
                $url = $this->getVarMypayApiUrl() . 'payment/mystore/cancel';
            } else if ($request->type == 'refund') {
                $url = $this->getVarMypayApiUrl() . 'payment/mystore/refund';
            }

            $params = [
                'transaction_number' => $Transaksi->no_ref,
            ];
            $headers = [
                'Accept: application/json',
                'X-Requested-With: XMLHttpRequest',
                'Authorization: ' . $request->header('Authorization'),
                'Content-Type: multipart/form-data'
            ];
            $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);

            if ($response['status'] == 200) {
                $Transaksi->status = '-1';
                if ($Transaksi->save()) {
                    if ($request->type == 'cancel') {

                        $title = "Status Transaksi #" . $Transaksi->no_ref;
                        $message = "Transaksi anda telah dibatalkan";
                        $idTransaction = $Transaksi->id;
                        $topics = $this->getVarPrefixNotif()['user'] . $User->phone;
                        FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                    } else if ($request->type == 'refund') {

                        $title = "Status Transaksi #" . $Transaksi->no_ref;
                        $message = "Transaksi anda dibatalkan oleh merchant";
                        $idTransaction = $Transaksi->id;
                        $topics = $this->getVarPrefixNotif()['user'] . $User->phone;
                        FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                    }

                    return $this->sendResponse('Pesanan berhasil dibatalkan', '');
                } else {
                    return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store (code: 0x002)');
                }
            } else {
                return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store (code: 0x003)');
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'cancelTransaction');
        }
    }

    public function updateProfile(Request $request, $id_merchant)
    {
        try {
            $request->validate([
                'nama_merchant' => 'required',
                'address' => 'required',
                'mdr' => 'required',
                'description' => 'required',
                'lat' => 'required',
                'lon' => 'required',
            ]);

            $findMerchant = Merchant::find($id_merchant);
            if ($findMerchant != null) {
                $findMerchant->nama_merchant = $request->nama_merchant;
                $findMerchant->address = $request->address;
                $findMerchant->mdr = $request->mdr;
                $findMerchant->description = $request->description;
                $findMerchant->lat = $request->lat;
                $findMerchant->lon = $request->lon;

                if ($request->hasFile('image')) {

                    $image = $request->file('image');
                    $path = public_path() . '/merchant_image/';
                    $filename = $findMerchant->id . '.' . $image->getClientOriginalExtension();
                    $image->move($path, $filename);
                    $findMerchant->image = $filename;
                }

                $findMerchant->save();

                return $this->sendResponse('User profile updated', new MerchantResource($findMerchant));
            } else {
                return $this->sendError('User not found');
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/updateProfile');
        }
    }

    public function qrcodeHistory($id_merchant)
    {
        try {
            $findMerchant = Merchant::find($id_merchant);
            if ($findMerchant == null) {
                return $this->sendError('Merchant not found');
            }

            $findUser = User::where('phone', $findMerchant->no_telp)->first();
            if ($findUser == null) {
                return $this->sendError('Merchant not found');
            }

            $findTransaction = TransactionMypay::where('description', 'Pembayaran ke Merchant')
            ->where('destination_id', $findUser->id)
            ->orderBy('created_at', 'desc')
            ->get();

            return $this->sendResponse('Transaction retrieved successfully', TransactionMypayResource::collection($findTransaction));
        } catch (\Exception $e) {
            return $this->sendException($e, 'MerchantController/qrcodeHistory');
        }
    }
}
