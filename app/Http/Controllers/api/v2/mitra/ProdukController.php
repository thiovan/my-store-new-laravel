<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\Produk;
use App\ProdukImage;
use App\ProdukDescription;
use Illuminate\Http\Request;
use App\Http\Functions\SkuGenerator;
use App\Http\Resources\ProdukResource;
use App\Http\Controllers\BaseController;

class ProdukController extends BaseController
{
    public function byMerchant($id_merchant)
    {
        try {
            $Produks = Produk::where('id_merchant', $id_merchant)->whereNotIn('id', $this->getVarExcludeProduct())->orderBy('priority', 'desc')->get();

            return $this->sendResponse('Merchant products retrieved successfully', ProdukResource::collection($Produks));
        } catch (\Exception $e) {
            return $this->sendException($e, 'ProdukController/byMerchant');
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'nama_produk' => 'required',
                'harga' => 'required',
                'id_merchant' => 'required',
                'id_kategori' => 'required',
                'id_parent' => 'required',
                'is_ready' => 'required',
                'status' => 'required',
                'deskripsi' => 'required',
            ]);

            if (Produk::where('id_merchant', $request->id_merchant)->where('nama_produk', $request->nama_produk)->first() != null) {
                return $this->sendError('Product name already exist');
            }

            if (!isset($request->sku)) {
                $sku = SkuGenerator::generate($request->id_kategori, $request->id_parent);
            } else {
                if (Produk::where('id_merchant', $request->id_merchant)->where('sku', $request->sku)->first() != null) {
                    return $this->sendError('SKU already exist');
                }
                $sku = $request->sku;
            }

            $addProduk = new Produk;
            $addProduk->nama_produk = \strtoupper($request->nama_produk);
            $addProduk->sku = $sku;
            $addProduk->harga = $request->harga;
            $addProduk->id_merchant = $request->id_merchant;
            $addProduk->id_kategori = $request->id_kategori;
            $addProduk->id_parent = $request->id_parent;
            $addProduk->is_ready = $request->is_ready;
            if ($request->status == 'in_apps') {
                $addProduk->in_apps = "y";
                $addProduk->in_physic = "n";
            } else if ($request->status == 'in_physic') {
                $addProduk->in_apps = "n";
                $addProduk->in_physic = "y";
            } else if ($request->status == 'both') {
                $addProduk->in_apps = "y";
                $addProduk->in_physic = "y";
            }
            if (!$addProduk->save()) {
                return $this->sendError('Failed add new product');
            }

            $addDescription = new ProdukDescription;
            $addDescription->id_produk = $addProduk->id;
            $addDescription->deskripsi = $request->deskripsi;
            $addDescription->save();

            if ($request->hasFile('images')) {

                $images = $request->file('images');
                for ($i = 0; $i < count($images); $i++) {

                    $path = public_path() . '/product_image/';
                    $filename = 'product_' . $addProduk->sku . '_' . $i . '.' . $images[$i]->getClientOriginalExtension();
                    $images[$i]->move($path, $filename);

                    $addImage = new ProdukImage;
                    $addImage->id_produk = $addProduk->id;
                    $addImage->original = $filename;
                    $addImage->save();
                }
            }

            return $this->sendResponse('Product added successfully', $addProduk);
        } catch (\Exception $e) {
            return $this->sendException($e, 'ProdukController/store');
        }
    }

    public function update($id_product, Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
            'harga' => 'required',
            'id_merchant' => 'required',
            'id_kategori' => 'required',
            'id_parent' => 'required',
            'is_ready' => 'required',
            'status' => 'required',
            'deskripsi' => 'required',
        ]);

        $findProduct = Produk::find($id_product);

        if ($findProduct != null) {
            $findProduct->nama_produk = $request->nama_produk;
            $findProduct->harga = $request->harga;
            $findProduct->id_merchant = $request->id_merchant;
            $findProduct->id_kategori = $request->id_kategori;
            $findProduct->id_parent = $request->id_parent;
            $findProduct->is_ready = $request->is_ready;
            if ($request->status == 'in_apps') {
                $findProduct->in_apps = "y";
                $findProduct->in_physic = "n";
            } else if ($request->status == 'in_physic') {
                $findProduct->in_apps = "n";
                $findProduct->in_physic = "y";
            } else if ($request->status == 'both') {
                $findProduct->in_apps = "y";
                $findProduct->in_physic = "y";
            }

            if (!$findProduct->save()) {
                return $this->sendError('Failed update product');
            }

            $findDescription = ProdukDescription::where('id_produk', $findProduct->id)->first();
            if ($findDescription != null) {
                $findDescription->id_produk = $findProduct->id;
                $findDescription->deskripsi = $request->deskripsi;
                $findDescription->save();
            }

            if ($request->hasFile('images')) {

                $images = $request->file('images');
                for ($i = 0; $i < count($images); $i++) {

                    $path = public_path() . '/product_image/';
                    $filename = 'product_' . $findProduct->sku . '_' . $i . '.' . $images[$i]->getClientOriginalExtension();
                    $images[$i]->move($path, $filename);

                    $findImage = ProdukImage::where('original', $filename)->first();
                    if ($findImage != null) {
                        $findImage->id_produk = $findProduct->id;
                        $findImage->original = $filename;
                        $findImage->save();
                    }
                }
            }
        }

        return $this->sendResponse('Product updated successfully', $findProduct);
    }
}
