<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\District;
use App\SubDistrict;
use App\MerchantRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Resources\DistrictResource;
use App\Http\Resources\MerchantRegionResource;

class RegionController extends BaseController
{
    public function districtAll()
    {
        try {
            $districtAll = District::all();

            return $this->sendResponse('District retrieved successfully', DistrictResource::collection($districtAll));
        } catch (\Exception $e) {
            return $this->sendException($e, 'RegionController/districtAll');
        }
    }

    public function showMerchantRegion($id_merchant)
    {
        try {
            $showMerchantRegion = MerchantRegion::where('id_merchant', $id_merchant)->get();

            return $this->sendResponse('Merchant region retrieved successfully', MerchantRegionResource::collection($showMerchantRegion));
        } catch (\Exception $e) {
            return $this->sendException($e, 'RegionController/indexMerchantRegion');
        }
    }

    public function createMerchantRegion(Request $request)
    {
        try {
            $request->validate([
                'id_merchant'   => 'required',
                'id_sub_district' => 'required',
            ]);

            $findMerchantRegion = MerchantRegion::where('id_merchant', $request->id_merchant)
                ->whereIn('id_sub_district', $request->id_sub_district)
                ->first();

            if ($findMerchantRegion != null) {
                $findSubDistrict = SubDistrict::find($findMerchantRegion->id_sub_district);
                return $this->sendError('Sub district ' . $findSubDistrict->display_name . ' already added');
            }

            foreach ($request->id_sub_district as $id_sub_district) {
                $createRegion = new MerchantRegion;
                $createRegion->id_merchant = $request->id_merchant;
                $createRegion->id_sub_district = $id_sub_district;
                $createRegion->save();
            }

            return $this->sendResponse('Region created successfully', '');
        } catch (\Exception $e) {
            return $this->sendException($e, 'RegionController/createMerchantRegion');
        }
    }

    public function deleteMerchantRegion($id)
    {
        try {
            $deleteRegion = MerchantRegion::destroy($id);
            if ($deleteRegion) {

                return $this->sendResponse('Region deleted successfully', ['id' => $id]);
            }

            return $this->sendError('Region failed to delete');
        } catch (\Exception $e) {
            return $this->sendException($e, 'RegionController/deleteMerchantRegion');
        }
    }
}
