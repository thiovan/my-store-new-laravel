<?php

namespace App\Http\Controllers\api\v2\mitra;

use App\Stok;
use App\Produk;
use Carbon\Carbon;
use App\TransaksiDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class StokController extends BaseController
{
    public function store(Request $request)
    {
        try {
            $storeStok = new Stok;
            $storeStok->id_produk = $request->id_produk;
            $storeStok->jumlah_stok = abs((int)$request->jumlah_stok);
            $storeStok->waktu = Carbon::now();
            $storeStok->ket = null;

            if (!$storeStok->save()) {
                return $this->sendError('Failed add stock');
            }

            return $this->sendResponse('Stock added successfully', $storeStok);
        } catch (\Exception $e) {
            return $this->sendException($e, 'StokController/store');
        }
    }

    public function returnStock(Request $request)
    {
        try {
            $productStock = Stok::where('id_produk', $request->id_produk)->sum('jumlah_stok') - TransaksiDetail::where('id_produk', $request->id_produk)->sum('jumlah');
            if ($request->jumlah_stok > $productStock) {
                $request->jumlah_stok = $productStock;
            }

            $returnStok = new Stok;
            $returnStok->id_produk = $request->id_produk;
            $returnStok->jumlah_stok = (int)$request->jumlah_stok <= 0 ? (int)$request->jumlah_stok : (int)-$request->jumlah_stok;
            $returnStok->waktu = Carbon::now();
            $returnStok->ket = $request->ket;

            if (!$returnStok->save()) {
                return $this->sendError('Failed return stock');
            }

            return $this->sendResponse('Stock returned successfully', $returnStok);
        } catch (\Exception $e) {
            return $this->sendException($e, 'StokController/returnStock');
        }
    }
}
