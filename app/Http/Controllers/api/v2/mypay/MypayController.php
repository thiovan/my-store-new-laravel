<?php

namespace App\Http\Controllers\api\v2\mypay;

use App\User;
use App\Merchant;
use App\Variable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseController;
use App\Http\Resources\MerchantResource;
use App\Http\Functions\FirebaseNotification;

class MypayController extends BaseController
{
    public function merchantNearby(Request $request)
    {
        $merchantNearby = DB::select("SELECT * , ROUND((6371 * 2 * ASIN(SQRT( POWER(SIN(( ? - lat) *  pi()/180 / 2), 2) +COS( ? * pi()/180) * COS(lat * pi()/180) * POWER(SIN(( ? - lon) * pi()/180 / 2), 2) ))), 2) as distance
        FROM merchant
        HAVING distance <= ?
        ORDER BY distance", [$request->lat, $request->lat, $request->lon, $request->distance]);

        return $this->sendResponse('Nearby merchant retrieved successfully', MerchantResource::collection(collect($merchantNearby)));
    }

    public function merchantByPhone($phone)
    {
        try {
            $findMerchant = Merchant::where('no_telp', $phone)->first();

            if ($findMerchant != null) {
                return $this->sendResponse('Merchant retrieved successfully', new MerchantResource($findMerchant));
            } else {
                return $this->sendError('Merchant not found');
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'MypayController/merchantByPhone');
        }
    }

    public function qrcodePaymentNotification(Request $request)
    {
        try {
            $request->validate([
                'sender_id' => 'required',
                'receiver_id' => 'required',
            ]);

            $findSender = User::with('profile')->find($request->sender_id);
            $findReceiver = User::with('profile')->find($request->receiver_id);

            $title = 'Pembayaran via QR Code';
            $inner_title = 'Dari: '.$findSender->profile->name;
            $message = '';
            $topics = $this->getVarPrefixNotif()['merchant'] . $findReceiver->phone;
            $response = FirebaseNotification::send($title, $inner_title, $message, $topics);
            // $response = "$title\n$inner_title\n$message\n$topics";

            return $this->sendResponse('Notification sent', $response);
        } catch (\Exception $e) {
            return $this->sendException($e, 'MypayController/qrcodePaymentNotification');
        }
    }
}
