<?php

namespace App\Http\Controllers\api\v2\mystore;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\CartResource;
use App\Http\Controllers\BaseController;

class CartController extends BaseController
{
    /**
     * List cart filtered by user
     *
     * @method GET
     * @param Integer id_user
     * @return JsonArray
     */
    public function user($id_user)
    {
        try {
            $Carts = Cart::where('machine', $id_user)->get();

            return $this->sendResponse('Shopping cart by user retrieved successfully', CartResource::collection($Carts));
        } catch (\Exception $e) {
            return $this->sendException($e, 'cart/user');
        }
    }

    /**
     * Get cart filtered by id
     *
     * @method GET
     * @param Array id_cart {example: 1,2,3}
     * @return JsonArray
     */
    public function id($id_cart)
    {
        try {
            $Carts = Cart::find(explode(',', $id_cart));

            return $this->sendResponse('Shopping cart by id retrieved successfully', CartResource::collection($Carts));
        } catch (\Exception $e) {
            return $this->sendException($e, 'cart/id');
        }
    }

    /**
     * Count cart filtered by id
     *
     * @method GET
     * @param Integer id_user
     * @return Integer cart count
     */
    public function count($id_user)
    {
        try {
            $cartCount = Cart::where('machine', $id_user)->count();

            return $this->sendResponse('Shopping cart count retrieved successfully', ['cart_count' => $cartCount]);
        } catch (\Exception $e) {
            return $this->sendException($e, 'cart/count');
        }
    }

    /**
     * Store new cart entry
     *
     * @method POST
     * @param Request {id_user, id_produk, jumlah, harga}
     */
    public function store(Request $request)
    {
        //Check parameter is satisfied
        $validator = Validator::make($request->all(), [
            'id_user'   => 'required|integer',
            'id_produk' => 'required|integer',
            'jumlah'    => 'required|integer',
            'harga'     => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Invalid parameter');
        };

        DB::beginTransaction();
        try {
            $Cart = Cart::where('machine', $request->id_user)->where('id_produk', $request->id_produk)->first();

            if ($Cart != null) {
                //Check product stock
                if ($this->productStock($request->id_produk) < ($request->jumlah + $Cart->jumlah)) {
                    return $this->sendError('Not enough product stock');
                }

                $Cart->jumlah = $Cart->jumlah + $request->jumlah;
                $Cart->save();
            } else {
                //Check product stock
                if ($this->productStock($request->id_produk) < $request->jumlah) {
                    return $this->sendError('Not enough product stock');
                }

                $Cart = new Cart;
                $Cart->id_produk = $request->id_produk;
                $Cart->harga = $request->harga;
                $Cart->jumlah = $request->jumlah;
                $Cart->machine = $request->id_user;
                $Cart->save();
            }

            DB::commit();
            return $this->sendResponse('Shopping cart stored successfully', new CartResource($Cart));
        } catch (\Exception $e) {

            DB::rollBack();
            return $this->sendException($e, 'cartStore');
        }
    }

    /**
     * Update jumlah in cart table
     *
     * @method PUT
     * @param Request {jumlah}, Integer id_cart
     * @return JsonObject
     */
    public function update(Request $request, $id_cart)
    {
        DB::beginTransaction();
        try {
            $Cart = Cart::find($id_cart);

            if ($Cart == null) {
                return $this->sendError('Cart not found', true, 404);
            }

            $Cart->jumlah = $request->jumlah;
            $Cart->save();
            DB::commit();

            return $this->sendResponse('Shopping cart updated successfully', new CartResource($Cart));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendException($e, 'cart/update');
        }
    }

    /**
     * Delete cart by id
     *
     * @method DELETE
     * @param Integer id_cart
     */
    public function delete($id_cart)
    {
        DB::beginTransaction();
        try {
            Cart::destroy($id_cart);
            DB::commit();

            return $this->sendResponse('Shopping cart deleted successfully', (object) []);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendException($e, 'cart/delete');
        }
    }
}
