<?php

namespace App\Http\Controllers\api\v2\mystore;

use App\Produk;
use App\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ProdukResource;
use App\Http\Controllers\BaseController;
use App\Http\Resources\MerchantResource;

class ProductController extends BaseController
{
    /**
     * Function helper to check product stock
     * @param Integer id_product
     * @return Integer product stock
     */
    public function _checkProductStock($id_product)
    {
        $productStock = Stok::where('id_produk', $id_product)
            ->sum('jumlah_stok');
        $productSold = TransaksiDetail::where('id_produk', $id_product)
            ->sum('jumlah');
        return $productStock - $productSold;
    }

    /**
     * Search product and merchant
     *
     * @method GET
     * @param  String keyword
     * @return JsonArray
     */
    public function search($keyword)
    {
        try {
            if ($keyword == 'SAMPLETEST123') {
                //Get sample product for testing purpose
                $resultProduks = Produk::where('id', '160')
                    ->get();
            } else {
                //Get product
                $resultProduks = Produk::where('nama_produk', 'like', '%' . $keyword . '%')
                    ->whereNotIn('id', $this->getVarExcludeProduct())
                    ->orderBy('priority', 'desc')
                    ->orderBy('id', 'desc')
                    ->get();
            }

            //Get merchant
            $resultMerchants = Merchant::where('nama_merchant', 'like', '%' . $keyword . '%')->get();

            //Combine collection
            $response = [
                'produk_result'      => ProdukResource::collection($resultProduks),
                'merchant_result'    => MerchantResource::collection($resultMerchants)
            ];

            return $this->sendResponse('Search result retrieved successfully', $response);
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/search');
        }
    }

    /**
     * List product filtered by category
     *
     * @method GET
     * @param Integer id_category
     * @return JsonArray
     */
    public function category($id_category)
    {
        try {
            $Produks = Produk::where('id_kategori', $id_category)
                ->whereNotIn('id', $this->getVarExcludeProduct())
                ->orderBy('priority', 'desc')
                ->orderBy('id', 'desc')
                ->get();

            return $this->sendResponse('Product list by category retrieved successfully', ProdukResource::collection($Produks));
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/category');
        }
    }

    /**
     * List product filtered by brand
     *
     * @method GET
     * @param Integer id_brand
     * @return JsonArray
     */
    public function brand($id_brand)
    {
        try {
            $Produks = Produk::where('id_parent', $id_brand)
                ->whereNotIn('id', $this->getVarExcludeProduct())
                ->orderBy('priority', 'desc')
                ->orderBy('id', 'desc')
                ->get();

            return $this->sendResponse('Product list by brand retrieved successfully', ProdukResource::collection($Produks));
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/brand');
        }
    }

    /**
     * Get product filtered by sku
     *
     * @method GET
     * @param String sku
     * @return JsonObject
     */
    public function sku($product_sku)
    {
        try {
            $Produk = Produk::where('sku', $product_sku)->first();

            if ($Produk != null) {
                return $this->sendResponse('Product show by sku retrieved successfully', new ProdukResource($Produk));
            } else {
                return $this->sendError('Product not found', true, 404);
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/sku');
        }
    }

    /**
     * Get product filtered by id_product
     *
     * @method GET
     * @param Integer id_product
     * @return JsonObject
     */
    public function id($id_product)
    {
        try {
            $Produk = Produk::find($id_product);

            if ($Produk != null) {
                return $this->sendResponse('Product show by id retrieved successfully', new ProdukResource($Produk));
            } else {
                return $this->sendError('Product not found', true, 404);
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/id');
        }
    }

    /**
     * List product filtered by id_merchant
     *
     * @method GET
     * @param Integer id_merchant
     * @return JsonArray
     */
    public function merchant($id_merchant)
    {
        try {
            $Produks = Produk::where('id_merchant', $id_merchant)
                ->whereNotIn('id', $this->getVarExcludeProduct())
                ->orderBy('priority', 'desc')
                ->orderBy('id', 'desc')
                ->get();

            return $this->sendResponse('Product list by merchant retrieved successfully', ProdukResource::collection($Produks));
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/merchant');
        }
    }

    /**
     * List best sales product
     *
     * @method GET
     * @return JsonArray
     */
    public function bestSales()
    {
        try {
            $BestSales = DB::table('produk')
                ->select(DB::raw('produk.*, (SELECT SUM(jumlah) FROM transaksidetail WHERE id_produk = produk.id) as penjualan'))
                ->whereNotIn('id', $this->getVarExcludeProduct())
                ->orderBy('penjualan', 'desc')
                ->limit(5)
                ->get();

            return $this->sendResponse('Best sales product retrieved successfully', ProdukResource::collection($BestSales));
        } catch (\Exception $e) {
            return $this->sendException($e, 'product/bestSales');
        }
    }

    /**
     * Update product
     *
     * @method PUT
     * @param Request {?nama_produk, ?harga, ?id_kategori, ?id_parent, ?sku}, Integer id_product
     * @return JsonObject
     */
    public function update(Request $request, $id_product)
    {
        try {
            $Produk = Produk::find($id_product);

            if ($Produk == null) {
                return $this->sendError('Product not found', true, 404);
            }

            isset($request->nama_produk) ? $Produk->nama_produk = $request->nama_produk : null;
            isset($request->harga) ? $Produk->harga = $request->harga : null;
            isset($request->id_kategori) ? $Produk->id_kategori = $request->id_kategori : null;
            isset($request->id_parent) ? $Produk->id_parent = $request->id_parent : null;

            //If not tstore product then update sku
            if (isset($request->sku) && substr($Produk->sku, 0, 2) != 'TS') {
                $Produk->sku = $request->sku;
            }

            DB::beginTransaction();
            try {
                $Produk->save();
                DB::commit();
                return $this->sendResponse('Product updated successfully', new ProdukResource($Produk));
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->sendError('Product store failed');
            }
        } catch (\Exception $e) {

            return $this->sendException($e, 'product/update');
        }
    }
}
