<?php

namespace App\Http\Controllers\api\v2\mystore;

use App\Cart;
use App\Stok;
use App\User;
use App\Produk;
use App\Merchant;
use App\Transaksi;
use Carbon\Carbon;
use App\DeliveryCost;
use App\TransaksiDetail;
use App\Http\Functions\Logs;
use Illuminate\Http\Request;
use App\Http\Functions\TrxCode;
use Illuminate\Support\Facades\DB;
use App\Http\Functions\CurlRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Http\Resources\TransaksiResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Functions\FirebaseNotification;

class TransactionController extends BaseController
{
    /**
     * Transaction Status
     * @var -1  => transaction cancelled
     * @var 0   => transaction added
     * @var 1   => transaction confirmed by merchant
     * @var 2   => transaction on delivery
     * @var 3   => transaction finished
     */

    /**
     * Get transaction filtered by id
     *
     * @method GET
     * @param Integer id_transaction
     * @return JsonObject
     */
    public function id($id_transaction)
    {
        try {
            $Transaksi = Transaksi::find($id_transaction);

            if ($Transaksi ==  null) {
                return $this->sendError('Transaction not found');
            }

            return $this->sendResponse('Transaction retrieved successfully', new TransaksiResource($Transaksi));
        } catch (\Exception $e) {
            return $this->sendException($e, 'transaction/id');
        }
    }

    /**
     * List history (finished or cancelled transaction) filtered by id_user
     *
     * @method GET
     * @param Integer id_user
     * @return JsonArray
     */
    public function history($id_user)
    {
        try {
            $Transaksis = Transaksi::where('machine', $id_user)
                ->whereIn('status', ['-1', '3'])
                ->orderBy('waktu', 'desc')
                ->get();

            return $this->sendResponse('Transaction history retrieved successfully', TransaksiResource::collection($Transaksis));
        } catch (\Exception $e) {
            return $this->sendException($e, 'transaction/history');
        }
    }

    /**
     * List order (progressing transaction) filtered by id_user
     *
     * @method GET
     * @param Integer id_user
     * @return JsonArray
     */
    public function order($id_user)
    {
        try {
            $Transaksis = Transaksi::where('machine', $id_user)
                ->whereIn('status', ['0', '1', '2'])
                ->orderBy('waktu', 'desc')
                ->get();

            return $this->sendResponse('Order retrieved successfully', TransaksiResource::collection($Transaksis));
        } catch (\Exception $e) {
            return $this->sendException($e, 'transaction/order');
        }
    }

    /**
     * Count order (progressing transaction) filtered by id_user
     *
     * @method GET
     * @param Integer id_user
     * @return Integer count order
     */
    public function orderCount($id_user)
    {
        try {
            $orderCount = Transaksi::where('machine', $id_user)
                ->whereIn('status', ['0', '1', '2'])
                ->orderBy('waktu', 'desc')
                ->count();

            return $this->sendResponse('Order retrieved successfully', ['order_count' => $orderCount]);
        } catch (\Exception $e) {
            return $this->sendException($e, 'orderCount');
        }
    }

    /**
     * Cancel transaction
     *
     * @method POST
     * @param Integer id_transaction, Enum type [cancel, refund]
     */
    public function cancel(Request $request)
    {
        try {
            //Check parameter is satisfied
            $validator = Validator::make($request->all(), [
                'id_transaction'    => 'required',
                'type'              => 'required|in:cancel,refund'
            ]);
            if ($validator->fails() || $request->header('Authorization') == null) {
                return $this->sendError('Invalid parameter');
            };

            $Transaksi = Transaksi::find($request->id_transaction);
            //Check if transaction exist
            if ($Transaksi == null) {
                return $this->sendError('Transaction not found', true, 404);
            }

            $User = User::find($Transaksi->machine);
            //Check id user exist
            if ($User == null) {
                return $this->sendError('User not found', true, 404);
            }

            //Check if transaction status already confirmed or cancelled
            if ($Transaksi->status != '0') {
                return $this->sendError('Transaction already confirmed or cancelled by merchant');
            }

            if ($request->type == 'cancel') {
                //User cancel transaction
                $url = $this->getVarMypayApiUrl() . 'payment/mystore/cancel';
            } else if ($request->type == 'refund') {
                //Merchant cancel transaction
                $url = $this->getVarMypayApiUrl() . 'payment/mystore/refund';
            }

            $params = [
                'transaction_number' => $Transaksi->no_ref,
            ];
            $headers = [
                'Accept: application/json',
                'X-Requested-With: XMLHttpRequest',
                'Authorization: ' . $request->header('Authorization'),
                'Content-Type: multipart/form-data'
            ];
            $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);

            if ($response['status'] == 200) {
                $Transaksi->status = '-1';
                if ($Transaksi->save()) {
                    if ($request->type == 'cancel') {

                        $title = "Status Transaksi #" . $Transaksi->no_ref;
                        $message = "Transaksi anda telah dibatalkan";
                        $idTransaction = $Transaksi->id;
                        $topics = $this->getVarPrefixNotif()['user'] . $User->phone;
                        FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                    } else if ($request->type == 'refund') {

                        $title = "Status Transaksi #" . $Transaksi->no_ref;
                        $message = "Transaksi anda dibatalkan oleh merchant";
                        $idTransaction = $Transaksi->id;
                        $topics = $this->getVarPrefixNotif()['user'] . $User->phone;
                        FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);
                    }

                    return $this->sendResponse('Pesanan berhasil dibatalkan', (onject)[]);
                } else {
                    return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store (code: 0x001)');
                }
            } else {
                return $this->sendError('Gagal membatalkan pesanan, coba lagi nanti atau hubungi admin My-Store (code: 0x002)');
            }
        } catch (\Exception $e) {
            return $this->sendException($e, 'cancelTransaction');
        }
    }

    /**
     * Create new transaction
     *
     * @method POST
     * @param Header Authorization
     * @param Array id_cart, Integer id_user, String address, String remark, String lat, String lon, String pin
     */
    public function store(Request $request)
    {
        try {
            //Check parameter is satisfied
            $validator = Validator::make($request->all(), [
                'id_cart'   => 'required',
                'id_user'   => 'required',
                'address'   => 'required',
                'remark'    => 'required',
                'lat'       => 'required',
                'lon'       => 'required',
            ]);
            if ($validator->fails() || $request->header('Authorization') == null) {
                return $this->sendError('Invalid parameter');
            };

            //LOGGING
            $logs = new Logs('transaction');
            $logs->generate('===REQUEST DATA===');
            $logs->generate('Id cart: ' . implode("|", $request->id_cart));
            $logs->generate('Id user: ' . $request->id_user);
            $logs->generate('Address: ' . $request->address);
            $logs->generate('Remark: ' . $request->remark);
            $logs->generate('Lat: ' . $request->lat);
            $logs->generate('Lon: ' . $request->lon);
            $logs->generate('===END REQUEST DATA===');
            //LOGGING

            //prepare data for looping
            $tempCarts = Cart::leftJoin('produk', 'cart.id_produk', '=', 'produk.id')
                ->leftJoin('merchant', 'produk.id_merchant', '=', 'merchant.id')
                ->whereIn('cart.id', $request->id_cart)
                ->get([
                    'merchant.no_telp as no_telp',
                    'produk.id_merchant as id_merchant',
                    'cart.harga as harga',
                    'cart.jumlah as jumlah',
                    'cart.id_produk as id_produk'
                ]);
            $tempUser = User::with('profile')->find($request->id_user);
            $tempMerchantPhone = [];
            $tempIdMerchant = [];
            $tempTotal = 0;

            //LOGGING
            $logs->generate('User balance before: ' . $tempUser->profile->balance);
            //LOGGING

            //looping through carts
            foreach ($tempCarts as $Cart) {
                array_push($tempMerchantPhone, $Cart->no_telp);
                array_push($tempIdMerchant, $Cart->id_merchant);
                $tempTotal = $tempTotal + ($Cart->harga * $Cart->jumlah);

                //check product stock is enough (only product with is_ready='y' checked)
                $productStock = Stok::where('id_produk', $Cart->id_produk)->sum('jumlah_stok') - TransaksiDetail::where('id_produk', $Cart->id_produk)->sum('jumlah');
                $isReady = Produk::find($Cart->id_produk)->is_ready;
                if ($isReady == 'y' && $Cart->jumlah > $productStock) {
                    //LOGGING
                    $logs->generate('===STOCK NOT ENOUGH===');
                    $logs->generate('Cart count: ' . $Cart->jumlah);
                    $logs->generate('Available stock: ' . $productStock);
                    $logs->generate('===END STOCK NOT ENOUGH===');
                    //LOGGING

                    return $this->sendError('Stok produk tidak cukup, mohon kurangi jumlah pembelian');
                }
            }

            //LOGGING
            $logs->generate('Price total: ' . $tempTotal);
            //LOGGING

            //remove duplicate value
            $tempIdMerchant = array_unique($tempIdMerchant);

            //check user pin is valid
            $url = $this->getVarMypayApiUrl() . 'auth/accounts/pin';
            $params = [
                'pin'    => $request->pin
            ];
            $headers = [
                'Accept: application/json',
                'X-Requested-With: XMLHttpRequest',
                'Authorization: ' . $request->header('Authorization'),
                'Content-Type: multipart/form-data'
            ];
            $response = CurlRequest::postWithHeader($url, $params, $headers);
            if ($response != 200) {
                //LOGGING
                $logs->generate('===PIN INVALID===');
                $logs->generate('Server response: ' . json_encode($response));
                $logs->generate('===END PIN INVALID===');
                //LOGGING

                return $this->sendError('Pin yang anda masukkan salah');
            }

            //check merchant is buy own product
            if (in_array($tempUser->phone, $tempMerchantPhone)) {
                //LOGGING
                $logs->generate('===MERCHANT BUY OWN PRODUCT===');
                $logs->generate('Buyer phone: ' . $tempUser->phone);
                $logs->generate('Merchant phone: ' . implode("|", $tempMerchantPhone));
                $logs->generate('===END MERCHANT BUY OWN PRODUCT===');
                //LOGGING

                return $this->sendError('Tidak diperbolehkan membeli produk anda sendiri');
            }

            //check user balance is enough
            if ($tempUser->profile->balance < $tempTotal) {
                //LOGGING
                $logs->generate('===BALANCE NOT ENOUGH===');
                $logs->generate('User balance: ' . $tempUser->profile->balance);
                $logs->generate('Price total: ' . $tempTotal);
                $logs->generate('===END BALANCE NOT ENOUGH===');
                //LOGGING

                return $this->sendError('Saldo anda tidak mencukupi');
            }

            $savedTransaction = [];
            foreach ($tempIdMerchant as $idMerchant) {
                DB::beginTransaction();
                try {
                    //LOGGING
                    $logs->generate('===HANDLING TRANSACTION PER MERCHANT===');
                    $logs->generate('Id merchant: ' . $idMerchant);
                    //LOGGING

                    $Carts = Cart::join('produk', 'cart.id_produk', '=', 'produk.id')
                        ->whereIn('cart.id', $request->id_cart)
                        ->where('produk.id_merchant', $idMerchant)
                        ->get(['cart.id as id', 'cart.harga as harga', 'cart.jumlah as jumlah', 'cart.id_produk as id_produk', 'produk.nama_produk as nama']);

                    $idCarts = [];
                    $nominal = 0;
                    foreach ($Carts as $Cart) {
                        $nominal = $nominal + ($Cart->harga * $Cart->jumlah);
                        array_push($idCarts, $Cart->id);
                    }

                    $DeliveryCost = DeliveryCost::where('id_merchant', $idMerchant)->first();

                    $Transaksi = new Transaksi;
                    $Transaksi->no_ref = TrxCode::generate($idMerchant);
                    $Transaksi->nominal = $nominal;
                    $Transaksi->waktu = Carbon::now();
                    $Transaksi->id_text = '0';
                    $Transaksi->id_courier = '0';
                    $Transaksi->address = $request->address;
                    if ($request->remark == null) {
                        $Transaksi->remark = '-';
                    } else {
                        $Transaksi->remark = $request->remark;
                    }
                    if ($DeliveryCost == null) {
                        $Transaksi->delivery_cost = 0;
                    } else {
                        $Transaksi->delivery_cost = $DeliveryCost->cost;
                    }
                    $Transaksi->status = '0';
                    $Transaksi->lat = $request->lat;
                    $Transaksi->lon = $request->lon;
                    $Transaksi->machine = $request->id_user;
                    $Transaksi->save();

                    //LOGGING
                    $logs->generate('Transaction created (id: ' . $Transaksi->id . ')');
                    //LOGGING

                    for ($i = 0; $i < count($Carts); $i++) {
                        $TransaksiDetail = new TransaksiDetail;
                        $TransaksiDetail->id_transaksi = $Transaksi->id;
                        $TransaksiDetail->id_produk = $Carts[$i]->id_produk;
                        $TransaksiDetail->harga = $Carts[$i]->harga;
                        $TransaksiDetail->jumlah = $Carts[$i]->jumlah;
                        $TransaksiDetail->waktu = $Transaksi->waktu;
                        $TransaksiDetail->machine = $request->id_user;
                        $TransaksiDetail->mdr = Merchant::find($idMerchant)->mdr;
                        $TransaksiDetail->save();

                        //LOGGING
                        $logs->generate('TransactionDetail created (id: ' . $TransaksiDetail->id . ')');
                        //LOGGING
                    }

                    //LOGGING
                    $logs->generate('Deduct user balance (' . $nominal . ')');
                    //LOGGING

                    //transfer to merchant
                    $Merchant = Merchant::find($idMerchant);
                    $url = $this->getVarMypayApiUrl() . 'payment/mystore';
                    $params = [
                        'merchant_number'   => $Merchant->no_telp,
                        'amount'            => (int) $nominal,
                        'pin'               => $request->pin,
                        'mdr'               => (int) $Merchant->mdr,
                        'ongkir'            => (int) $Transaksi->delivery_cost
                    ];
                    $headers = [
                        'Accept: application/json',
                        'X-Requested-With: XMLHttpRequest',
                        'Authorization: ' . $request->header('Authorization'),
                        'Content-Type: multipart/form-data'
                    ];
                    $response = CurlRequest::postMultipartWithHeader($url, $params, $headers);
                    if ($response['status'] != 200) {
                        //LOGGING
                        $logs->generate('Deduct failed (response: ' . json_encode($response) . ')');
                        //LOGGING

                        DB::rollBack();
                        return $this->sendError('Gagal memotong saldo, mohon coba beberapa saat lagi');
                    }

                    //update no_ref generated after deduct balance
                    $Transaksi = Transaksi::find($Transaksi->id);
                    $Transaksi->no_ref = $response['response']->transaction_number;
                    $Transaksi->save();

                    //send notification to buyer
                    $title = 'Status Transaksi #' . $Transaksi->no_ref;
                    $message = 'Transaksi anda sedang diproses';
                    $idTransaction = $Transaksi->id;
                    $topics = $this->getVarPrefixNotif()['user'] . $tempUser->phone;
                    FirebaseNotification::sendOrder($title, $message, $idTransaction, $topics);

                    //send notification to merchant
                    $title = 'Order Masuk #' . $Transaksi->no_ref;
                    $inner_title = 'Transaksi menunggu konfirmasi';
                    $message = '';
                    $topics = $this->getVarPrefixNotif()['merchant'] . $Merchant->no_telp;
                    FirebaseNotification::send($title, $inner_title, $message, $topics);

                    Cart::destroy($idCarts);
                    $logs->generate('===END HANDLING TRANSACTION PER MERCHANT===');
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    throw $e;
                }
            }

            //LOGGING
            $logs->generate('===TRANSACTION SUCCESS===');
            //LOGGING

            return $this->sendResponse('Transaction stored successfully', (object) []);
        } catch (\Exception $e) {
            return $this->sendException($e, 'transaction/store');
        }
    }
}
