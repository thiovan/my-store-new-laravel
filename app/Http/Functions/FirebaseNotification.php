<?php

namespace App\Http\Functions;

use Carbon\Carbon;
use App\Http\Controllers\BaseController;

class FirebaseNotification
{

	public static function send($title, $inner_title, $message, $topics)
	{
        $baseController = new BaseController();

		$fp = fopen('notification.log', 'a');
		fwrite($fp, Carbon::now() . PHP_EOL);
		fwrite($fp, 'Notification Send' . PHP_EOL);
		fwrite($fp, 'Title: ' . $title . PHP_EOL);
		fwrite($fp, 'Inner Title: ' . $inner_title . PHP_EOL);
		fwrite($fp, 'Message: ' . $message . PHP_EOL);
		fwrite($fp, 'Topic: ' . $topics . PHP_EOL . PHP_EOL . PHP_EOL);
		fclose($fp);

		if ($baseController->getVarFirebaseNotification()) {

			$FIREBASE_API_KEY = $baseController->getVarFirebaseApiKey();

			$data = array
			(
				'title' 				=> $title,
				'inner_title' 			=> $inner_title,
				'message' 				=> $message,

			);

			$fields = array
			(
				'to'        			=> '/topics/' . $topics,
				'data' 					=> $data,
				'priority'				=> 'high',
				'show_in_foreground'	=> true,
				'content_available'		=> true
			);

			$headers = array
			(
				'Authorization: key=' . $FIREBASE_API_KEY,
				'Content-Type: application/json'
			);

			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );

			return $result;

		}

		return false;
	}

	public static function sendPromotion($title, $message, $image, $topics)
	{
        $baseController = new BaseController();

		$fp = fopen('notification.log', 'a');
		fwrite($fp, Carbon::now() . PHP_EOL);
		fwrite($fp, 'Notification Promotion Send' . PHP_EOL);
		fwrite($fp, 'Title: ' . $title . PHP_EOL);
		fwrite($fp, 'Message: ' . $message . PHP_EOL);
		fwrite($fp, 'Image: ' . $image . PHP_EOL);
		fwrite($fp, 'Topic: ' . $topics . PHP_EOL . PHP_EOL . PHP_EOL);
		fclose($fp);

		if ($baseController->getVarFirebaseNotification()) {

			$FIREBASE_API_KEY = $baseController->getVarFirebaseApiKey();

			$data = array
			(
				'title' 				=> $title,
				'message' 				=> $message,
				'image'					=> $image,
				'notification_type'		=> 'promotion'

			);

			$fields = array
			(
				'to'        			=> '/topics/' . $topics,
				'data' 					=> $data,
				'priority'				=> 'high',
				'show_in_foreground'	=> true,
				'content_available'		=> true
			);

			$headers = array
			(
				'Authorization: key=' . $FIREBASE_API_KEY,
				'Content-Type: application/json'
			);

			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );

			return $result;

		}

		return false;
	}

	public static function sendOrder($title, $message, $id_transaction, $topics)
	{
        $baseController = new BaseController();

		$fp = fopen('notification.log', 'a');
		fwrite($fp, Carbon::now() . PHP_EOL);
		fwrite($fp, 'Notification Order Send' . PHP_EOL);
		fwrite($fp, 'Title: ' . $title . PHP_EOL);
		fwrite($fp, 'Message: ' . $message . PHP_EOL);
		fwrite($fp, 'Id Transaction: ' . $id_transaction . PHP_EOL);
		fwrite($fp, 'Topic: ' . $topics . PHP_EOL . PHP_EOL . PHP_EOL);
		fclose($fp);

		if ($baseController->getVarFirebaseNotification()) {

			$FIREBASE_API_KEY = $baseController->getVarFirebaseApiKey();

			$data = array
			(
				'title' 				=> $title,
				'message' 				=> $message,
				'id_transaction'		=> $id_transaction,
				'notification_type'		=> 'order'

			);

			$fields = array
			(
				'to'        			=> '/topics/' . $topics,
				'data' 					=> $data,
				'priority'				=> 'high',
				'show_in_foreground'	=> true,
				'content_available'		=> true
			);

			$headers = array
			(
				'Authorization: key=' . $FIREBASE_API_KEY,
				'Content-Type: application/json'
			);

			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );

			return $result;

		}

		return false;
	}
}
