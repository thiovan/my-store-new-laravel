<?php

namespace App\Http\Functions;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class Logs
{
    public $uniqueId;
    public $logName;
    public $dateString;

    public function __construct(string $logName, string $uniqueId = null)
    {
        if ($uniqueId == null) {
            $this->uniqueId = uniqid();
        } else {
            $this->uniqueId = $uniqueId;
        }
        $this->logName = $logName;
        $this->dateString = Carbon::now()->toDateString();
    }

    public function generate(string $message)
    {
        $dir = $this->logName . '_logs/' . $this->dateString . '/' . $this->uniqueId . '.log';
        $message = '[' . Carbon::now()->toDateTimeString() . '] ' . $message;
        Storage::append($dir, $message);
    }
}
