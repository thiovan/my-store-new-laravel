<?php

namespace App\Http\Functions;

use App\Transaksi;

use Carbon\Carbon;

class TrxCode
{
	public static function generate($id_merchant)
	{
		return TrxCode::enc(date("YmdHis")).TrxCode::enc($id_merchant);	
	}

	public static function enc($val)
	{
		$arr=array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

		$bagi=$val;
		$code="";
		$len=count($arr);
		while($bagi>$len){
			$mod=$bagi%$len;
			$code=$arr[$mod].$code;
			$res=$bagi/$len;
			$resarr=explode(".", $res);
			$bagi=$resarr[0];
		}
		
		return $code;
	}
}