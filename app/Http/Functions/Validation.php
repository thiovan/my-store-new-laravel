<?php

namespace App\Http\Functions;

use Validator;

class Validation
{
	public static function isValid($request, $params)
	{
		$input = $request->all();
		$rules = [];

		foreach ($params as $key => $value) {

			$temp = [$value => 'required'];
			$rules = array_merge($rules, $temp);

		}

		$Validator = Validator::make($input, $rules);

		if(!$Validator->fails()){

			return true;
			
		}

		return false;
	}
}