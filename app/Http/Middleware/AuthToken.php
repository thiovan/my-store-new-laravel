<?php

namespace App\Http\Middleware;

use Closure;

use App\User;

use App\Http\Functions\CurlRequest;

class AuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        // $url = $this->getVarMypayApiUrl() . 'user/auth_token';
        // $params = [
        //     'token' => $token
        // ];

        // $response = CurlRequest::post($url, $params);

        // if ($response['status'] != 200) {
        //     $response = [
        //         'message' => 'Unauthorized'
        //     ];

        //     return response()->json($response, 401);
        // }else{
        //     $request->merge([
        //         'mypay' => (object)$response['result']
        //     ]);

        //     return $next($request);
        // }

        $User = User::where('token', $token)->first();
        if ($User == null) {
            $response = [
                'message' => 'Unauthorized'
            ];

            return response()->json($response, 401);
        }else{
            $request->merge([
                'mypay' => (object)[
                    'iduser'    => $User->iduser,
                    'nohp'      => $User->nohp,
                    'token'     => $User->token
                ]
            ]);


            return $next($request);
        }
    }
}
