<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'banner'        => url('banner=' . rand(200,800) . $this->id . rand(20,80)),
            'id_merchant'   => $this->id_merchant,
            'merchant_name' => $this->merchant_name,
        ];
    }
}
