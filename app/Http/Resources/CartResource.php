<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Produk;
use App\DeliveryCost;

use App\Http\Resources\ProdukResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Produk = Produk::find($this->id_produk);
        $DeliveryCost = DeliveryCost::where('id_merchant', $Produk->id_merchant)->first();
        if ($DeliveryCost == null) {
            $cost = 0;
        }else{
            $cost = $DeliveryCost->cost;
        }

        return [
            'id'            => $this->id,
            'produk'        => new ProdukResource($Produk),
            'harga'         => $this->harga,
            'jumlah'        => $this->jumlah,
            'id_user'       => $this->machine,
            'delivery_cost' => $cost
        ];
    }
}
