<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Merchant;

class CourierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Merchant = Merchant::find($this->id_merchant);

        return [
            'id'            => $this->id,
            'username'      => $this->username,
            'nama'          => $this->nama,
            'nohp'          => $this->nohp,
            'email'         => $this->email,
            'is_active'     => $this->is_active,
            'lat'           => $this->lat,
            'lon'           => $this->lon,
            'radius'        => $this->radius,
            'merchant'      => new MerchantResource($Merchant)
        ];
    }
}
