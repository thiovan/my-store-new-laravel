<?php

namespace App\Http\Resources;

use App\SubDistrict;
use Illuminate\Http\Resources\Json\JsonResource;

class DistrictResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $findSubDistrict = SubDistrict::where('id_district', $this->id)->get();
        return [
            'id'            => $this->id,
            'district'      => $this->district,
            'display_name'  => $this->display_name,
            'sub_district'  => $findSubDistrict
        ];
    }
}
