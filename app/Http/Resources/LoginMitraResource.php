<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginMitraResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->login_as == 'merchant') {
            return [
                'id'        => $this->id,
                'username'  => $this->username,
                'nama'      => $this->nama_merchant,
                'address'   => $this->address,
                'phone'      => $this->no_telp,
                'email'     => $this->email,
                'is_active' => $this->is_active,
                'privilege' => 'merchant',
                'token'     => $this->token
            ];
        }else{
            return [
                'id'        => $this->id,
                'username'  => $this->username,
                'nama'      => $this->nama,
                'address'   => '',
                'phone'      => $this->nohp,
                'email'     => $this->email,
                'is_active' => $this->is_active,
                'privilege' => 'courier',
                'token'     => $this->token
            ];
        }

    }
}
