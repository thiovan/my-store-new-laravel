<?php

namespace App\Http\Resources;

use App\SubDistrict;
use Illuminate\Http\Resources\Json\JsonResource;

class MerchantRegionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $findSubDistrict = SubDistrict::find($this->id_sub_district);

        return [
            'id'        => $this->id,
            'region'    => new SubDistrictResource($findSubDistrict)
        ];
    }
}
