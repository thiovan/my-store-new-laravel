<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MerchantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $responseArray = [
            'id'            => $this->id,
            'username'      => $this->username,
            'nama_merchant' => $this->nama_merchant,
            'no_telp'       => $this->no_telp,
            'address'       => $this->address,
            'is_active'     => $this->is_active,
            'email'         => $this->email,
            'mdr'           => $this->mdr,
            'description'   => $this->description,
            'image'         => url('merchant/image=' . rand(200,800) . $this->id . rand(20,80)),
            'lat'           => $this->lat,
            'lon'           => $this->lon
        ];

        if (isset($this->distance)) {
            $responseArray['distance'] = $this->distance;
        }

        return $responseArray;
    }
}
