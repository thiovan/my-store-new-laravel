<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProdukImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'thumbnail' => url('product/thumbnail=' . rand(200,800) . $this->id . rand(20,80)),
            'original'  => url('product/image=' . rand(200,800) . $this->id . rand(20,80))
        ];
    }
}
