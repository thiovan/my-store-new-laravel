<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Merchant;
use App\Kategori;
use App\Brand;
use App\Discount;
use App\ProdukImage;
use App\ProdukDescription;
use App\Stok;
use App\TransaksiDetail;
use App\Variable;

use App\Http\Resources\MerchantResource;
use App\Http\Resources\KategoriResource;
use App\Http\Resources\BrandResource;
use App\Http\Resources\DiscountResource;
use App\Http\Resources\ProdukImageResource;
use App\Http\Resources\ProdukDescriptionResource;

class ProdukResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Merchant = Merchant::find($this->id_merchant);
        $Kategori = Kategori::find($this->id_kategori);
        $Brand = Brand::find($this->id_parent);
        $Discount = Discount::where('id_produk', $this->id)->first();
        $ProdukImage = ProdukImage::where('id_produk', $this->id)->get();
        if ($this->is_ready == 'n') {
            $productStock = 99;
        }else{
            $productStock = Stok::where('id_produk', $this->id)->sum('jumlah_stok') - TransaksiDetail::where('id_produk', $this->id)->sum('jumlah');
        } 

        $persentase = $Merchant->mdr;
        $harga_awal = $this->harga*(($persentase+100)/100);
        if ($Discount != null) {
            $harga_akhir = $this->harga*(($persentase+100)/100)*((100-$Discount->discount)/100);
            if ($Discount->label != '0') {
                $label = $Discount->label;
            }else{
                $label = '';
            }
        }else{
            $harga_akhir = $harga_awal;
            $label = '';
        }

        $description = '';
        $ProdukDescription = ProdukDescription::where('id_produk', $this->id)->first();
        if ($ProdukDescription != null) {
            $description = $ProdukDescription->deskripsi;
        }

        return [
            'id'            => $this->id,
            'sku'           => $this->sku,
            'nama_produk'   => $this->nama_produk,
            'harga_merchant'=> $this->harga,
            'harga_awal'    => round($harga_awal),
            'harga_akhir'   => round($harga_akhir),
            'merchant'      => new MerchantResource($Merchant),
            'kategori'      => new KategoriResource($Kategori),
            'brand'         => new BrandResource($Brand),
            'priority'      => $this->priority,
            'is_ready'      => $this->is_ready,
            'in_apps'       => $this->in_apps,
            'in_physic'     => $this->in_physic,
            'description'   => $description,
            'label'         => $label,
            'stock'         => $productStock,
            'image'         => ProdukImageResource::collection($ProdukImage)
        ];
    }
}
