<?php

namespace App\Http\Resources;

use App\District;
use Illuminate\Http\Resources\Json\JsonResource;

class SubDistrictResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $findDistrict = District::find($this->id_district);

        return [
            'district'          => $findDistrict,
            'sub_district'      => [
                'id'                => $this->id,
                'sub_district'      => $this->sub_district,
                'display_name'    => $this->display_name,
            ],
        ];
    }
}
