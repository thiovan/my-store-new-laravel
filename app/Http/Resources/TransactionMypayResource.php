<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionMypayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $findUser = User::find($this->user_id);
        $findDestination = User::find($this->destination_id);

        $arrResponse = parent::toArray($request);
        $arrResponse['user_id'] = $findUser;
        $arrResponse['destination_id'] = $findDestination;

        return $arrResponse;
    }
}
