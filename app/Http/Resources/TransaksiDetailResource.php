<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Produk;

use App\Http\Resources\ProdukResource;

class TransaksiDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Produk = Produk::find($this->id_produk);

        return [
            'id'            => $this->id,
            'id_transaksi'  => $this->id_transaksi,
            'produk'        => new ProdukResource($Produk),
            'harga'         => $this->harga,
            'jumlah'        => $this->jumlah,
            'waktu'         => $this->waktu,
            'machine'       => $this->machine
        ];
    }
}
