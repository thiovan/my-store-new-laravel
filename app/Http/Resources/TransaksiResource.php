<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Produk;
use App\TransaksiDetail;
use App\Courier;
use App\User;
use App\Review;

class TransaksiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $TransaksiDetails = TransaksiDetail::where('id_transaksi', $this->id)->get();
        $Courier = Courier::find($this->id_courier);
        $User = User::where('id', $this->machine)->first();
        $Review = Review::where('id_transaksi', $this->id)->first();
        
        return [
            'id'            => $this->id,
            'no_ref'        => $this->no_ref,
            'nominal'       => $this->nominal,
            'waktu'         => (string)$this->waktu,
            'id_text'       => $this->id_text,
            'courier'       => new CourierResource($Courier),
            'address'       => $this->address,
            'remark'        => $this->remark,
            'delivery_cost' => $this->delivery_cost,
            'status'        => $this->status,
            'lat'           => $this->lat,
            'lon'           => $this->lon,
            'customer'      => new UserResource($User),
            'persentase'    => $this->persentase,
            'review'        => $Review,
            'items'         => TransaksiDetailResource::collection($TransaksiDetails),
        ];
    }
}
