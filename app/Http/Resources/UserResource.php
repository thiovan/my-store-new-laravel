<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'phone'         => $this->phone,
            'name'          => $this->profile->name,
            'email'         => $this->email,
            'is_verified'   => $this->is_verified
        ];
    }
}
