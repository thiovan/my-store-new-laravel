<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantRegion extends Model
{
    protected $table = 'merchant_region';
    public $timestamps = false;

}
