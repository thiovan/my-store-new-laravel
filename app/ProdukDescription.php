<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukDescription extends Model
{
    protected $table = 'produk_description';
    public $timestamps = false;
}
