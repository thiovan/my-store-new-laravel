<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextMessage extends Model
{
    protected $table = 'text_message';
    public $timestamps = false;
}
