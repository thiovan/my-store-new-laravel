<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionMypay extends Model
{
    protected $table = 'transactions';
    protected $connection = 'mysql_mypay';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
