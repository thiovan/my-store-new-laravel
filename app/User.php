<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $connection = 'mysql_mypay';
    protected $hidden = ['password', 'pin'];

    public function profile()
    {
        return $this->hasOne('App\Profile', 'user_id', 'id');
    }
}
