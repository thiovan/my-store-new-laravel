<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
	protected $table = 'var';
	public $timestamps = false;
}
