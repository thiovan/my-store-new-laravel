<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courier', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_merchant')->nullable();
			$table->string('username', 50)->nullable();
			$table->string('password', 32)->nullable();
			$table->string('nama', 50)->nullable();
			$table->string('nohp', 15)->nullable();
			$table->string('email', 100)->nullable();
			$table->enum('is_active', array('y','n'))->default('n');
			$table->string('lat', 15)->nullable();
			$table->string('lon', 15)->nullable();
			$table->integer('radius')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courier');
	}

}
