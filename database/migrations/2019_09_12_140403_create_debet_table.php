<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDebetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('debet', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_merchant')->default(0);
			$table->integer('amount')->default(0);
			$table->date('waktu');
			$table->dateTime('waktu_input');
			$table->string('admin_phone', 15);
			$table->string('trx_number', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('debet');
	}

}
