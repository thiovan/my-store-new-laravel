<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMachineTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('machine', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama_machine', 100)->default('0');
			$table->string('username', 50);
			$table->string('password', 32);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('machine');
	}

}
