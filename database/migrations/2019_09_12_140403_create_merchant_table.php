<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('merchant', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('username', 100)->default('0');
			$table->string('password', 32)->default('0');
			$table->string('nama_merchant', 100)->default('0');
			$table->string('no_telp', 15)->default('0');
			$table->string('address', 400)->default('0');
			$table->enum('is_active', array('y','n'))->default('n');
			$table->string('email', 50)->nullable();
			$table->float('mdr', 4)->default(0.00);
			$table->text('description', 65535)->nullable();
			$table->string('image')->nullable();
			$table->float('lat', 10, 0)->nullable();
			$table->float('lon', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('merchant');
	}

}
