<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parent', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama_parent', 100)->default('0');
			$table->integer('id_kategori');
			$table->integer('id_merchant')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parent');
	}

}
