<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdukTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produk', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sku', 20)->default('0')->unique('sku');
			$table->string('nama_produk', 100)->default('0');
			$table->integer('harga')->default(0);
			$table->integer('id_merchant')->default(0);
			$table->integer('id_kategori')->default(0);
			$table->integer('id_parent')->default(0);
			$table->integer('priority')->default(0);
			$table->enum('is_ready', array('y','n'))->default('y');
			$table->enum('in_apps', array('y','n'))->nullable()->default('y');
			$table->enum('in_physic', array('y','n'))->nullable()->default('y');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produk');
	}

}
