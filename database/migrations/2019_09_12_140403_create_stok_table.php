<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStokTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stok', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_produk')->default(0);
			$table->integer('jumlah_stok')->default(0);
			$table->dateTime('waktu')->nullable();
			$table->string('ket', 500)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stok');
	}

}
