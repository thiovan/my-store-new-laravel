<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('text_message', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('updatetime')->nullable();
			$table->string('sender', 100)->nullable();
			$table->string('time', 100)->nullable();
			$table->string('text', 200)->nullable();
			$table->enum('is_processed', array('y','n'))->default('n');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('text_message');
	}

}
