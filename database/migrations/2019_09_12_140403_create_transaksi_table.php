<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaksiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('no_ref', 15)->default('0');
			$table->integer('nominal')->default(0);
			$table->dateTime('waktu')->nullable();
			$table->integer('id_text')->default(0);
			$table->integer('id_courier')->default(0);
			$table->text('address', 65535)->nullable();
			$table->text('remark', 65535)->nullable();
			$table->integer('delivery_cost')->nullable();
			$table->integer('status')->default(0);
			$table->string('lat', 50)->nullable();
			$table->string('lon', 50)->nullable();
			$table->string('machine', 50)->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksi');
	}

}
