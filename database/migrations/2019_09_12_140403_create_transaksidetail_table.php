<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaksidetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksidetail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_transaksi')->default(0);
			$table->integer('id_produk')->default(0);
			$table->integer('harga')->default(0);
			$table->integer('jumlah')->default(0);
			$table->dateTime('waktu')->nullable();
			$table->string('machine', 50)->nullable();
			$table->float('mdr', 4)->default(0.00);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksidetail');
	}

}
