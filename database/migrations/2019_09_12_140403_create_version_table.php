<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVersionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('version', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('latest_version')->nullable();
			$table->integer('latest_version_code')->nullable();
			$table->string('url')->nullable();
			$table->text('release_notes', 65535)->nullable();
			$table->dateTime('datetime')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('version');
	}

}
