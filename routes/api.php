<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('authToken')->group(function () { });

//Only For Testing
Route::any('coba', 'MainController@coba');

//Product Route
Route::get('product/search/{keyword}', 'MainController@productSearch'); // Deprecated
Route::get('product/category/{id_kategori}', 'MainController@productByCategory'); // Deprecated
Route::get('product/brand/{id_brand}', 'MainController@productByBrand'); // Deprecated
Route::get('product/sku/{sku}', 'MainController@productBySKU'); // Deprecated
Route::get('product/{id_produk}', 'MainController@productById'); // Deprecated
Route::get('product/merchant/{id_merchant}', 'MainController@productByMerchant'); // Deprecated
Route::put('product/{id_produk}', 'MainController@productUpdate'); // Deprecated
Route::get('product/best/sales', 'MainController@productBestSales'); // Deprecated
Route::delete('product/{id_produk}', 'MainController@productDelete');

//Cart Route
Route::get('cart/user/{id_user}', 'MainController@cartByUser'); // Deprecated
Route::get('cart/{id_cart}', 'MainController@cartById'); // Deprecated
Route::post('cart', 'MainController@cartStore'); // Deprecated
Route::put('cart/{id_cart}', 'MainController@cartUpdate'); // Deprecated
Route::delete('cart/{id_cart}', 'MainController@cartDelete'); // Deprecated
Route::get('cart/count/{id_user}', 'MainController@cartCount'); // Deprecated

//Transaction Route
Route::post('transaction', 'MainController@transactionStore'); // Deprecated
Route::get('transaction/{id_transaction}', 'MainController@transactionById'); // Deprecated
Route::post('transaction/cancel', 'MainController@transactionCancel'); // Deprecated
Route::get('history/{id_user}', 'MainController@history'); // Deprecated
Route::get('order/{id_user}', 'MainController@order'); // Deprecated
Route::get('order/count/{id_user}', 'MainController@orderCount'); // Deprecated
Route::post('review', 'MainController@reviewStore');

//Mitra login register
Route::post('mitra/login', 'MainController@loginMitra'); // Deprecated
Route::post('mitra/register', 'MainController@registerMitra'); // Deprecated

//Merchant Route
Route::post('merchant', 'MainController@merchantStore');
Route::get('confirmation/merchant={id_merchant}', 'MainController@confirmationListMerchant'); // Deprecated
Route::post('merchant/confirm', 'MainController@merchantConfirmTransaction'); // Deprecated
Route::get('history/merchant/{id_merchant}', 'MainController@merchantHistory'); // Deprecated

//Brand Route
Route::get('brand', 'MainController@brandList'); // Deprecated
Route::post('brand', 'MainController@brandStore'); // Deprecated

//Category Route
Route::get('category', 'MainController@categoryList'); // Deprecated
Route::post('category', 'MainController@categoryStore'); // Deprecated

//Courier Route
Route::post('courier/confirm/all', 'MainController@courierConfirmAll'); // Deprecated
Route::post('courier/confirm', 'MainController@courierConfirmTransaction'); // Deprecated
Route::get('courier/merchant={id_merchant}', 'MainController@courierListByMerchant'); // Deprecated
Route::get('confirmation/courier={id_courier}', 'MainController@confirmationListCourier'); // Deprecated
Route::get('history/courier/{id_courier}', 'MainController@courierHistory'); // Deprecated

//Other Route
Route::get('version/latest', 'MainController@latestVersion');
Route::get('banner', 'MainController@bannerList');
Route::post('user/status', 'MainController@userStatus');

//Bridge
Route::get('merchant/number={merchant_number}', 'MainController@bridgeMerchant');
Route::post('payment/success', 'MainController@bridgePayment');
Route::post('change/phone', 'MainController@bridgeChangeNoHP');
Route::get('var/percentage', 'MainController@bridgeVarPercentage');



// New API v2
Route::group(['prefix' => 'v2'], function () {
    //Just for testing purpose
    Route::get('playground', 'api\v2\PlaygroundController@index');

    //Routing My-Store Apps
    Route::group(['prefix' => 'mystore'], function () {
        Route::group(['prefix' => 'product'], function () {
            Route::get('search/{keyword}', 'api\v2\mystore\ProductController@search');
            Route::get('category/{id_category}', 'api\v2\mystore\ProductController@category')->where('id_category', '[0-9]+');
            Route::get('brand/{id_category}', 'api\v2\mystore\ProductController@brand')->where('id_category', '[0-9]+');
            Route::get('id/{id_product}', 'api\v2\mystore\ProductController@id')->where('id_product', '[0-9]+');
            Route::get('merchant/{id_merchant}', 'api\v2\mystore\ProductController@merchant')->where('id_merchant', '[0-9]+');
            Route::get('sku/{product_sku}', 'api\v2\mystore\ProductController@sku');
            Route::get('best/sales', 'api\v2\mystore\ProductController@bestSales');
            Route::put('update/{id_product}', 'api\v2\mystore\ProductController@update');
        });

        Route::group(['prefix' => 'cart'], function () {
            Route::get('user/{id_user}', 'api\v2\mystore\CartController@user')->where('id_user', '[0-9]+');
            Route::get('count/{id_cart}', 'api\v2\mystore\CartController@count')->where('id_user', '[0-9]+');
            Route::get('id/{id_cart}', 'api\v2\mystore\CartController@id');
            Route::post('store', 'api\v2\mystore\CartController@store');
            Route::put('update/{id_cart}', 'api\v2\mystore\CartController@update')->where('id_cart', '[0-9]+');
            Route::delete('delete/{id_cart}', 'api\v2\mystore\CartController@delete')->where('id_cart', '[0-9]+');
        });

        Route::group(['prefix' => 'transaction'], function () {
            Route::get('id/{id_transaction}', 'api\v2\mystore\TransactionController@id')->where('id_transaction', '[0-9]+');
            Route::get('history/{id_user}', 'api\v2\mystore\TransactionController@history')->where('id_user', '[0-9]+');
            Route::get('order/{id_user}', 'api\v2\mystore\TransactionController@order')->where('id_user', '[0-9]+');
            Route::get('order/count/{id_user}', 'api\v2\mystore\TransactionController@orderCount')->where('id_user', '[0-9]+');
            Route::post('store', 'api\v2\mystore\TransactionController@store');
            Route::post('cancel', 'api\v2\mystore\TransactionController@cancel');
        });
    });

    //Routing My-Pay Apps and Bridging
    Route::group(['prefix' => 'mypay'], function () {

        Route::post('merchant/nearby', 'api\v2\mypay\MypayController@merchantNearby');
        Route::get('merchant/phone/{phone}', 'api\v2\mypay\MypayController@merchantByPhone');
        Route::post('notif/qrcode', 'api\v2\mypay\MypayController@qrcodePaymentNotification');
    });

    //Routing Mitra My-Store Apps
    Route::group(['prefix' => 'mitra'], function () {

        Route::group(['prefix' => 'auth'], function () {
            Route::post('login', 'api\v2\mitra\AuthController@login');
            Route::post('register', 'api\v2\mitra\AuthController@register');
            Route::post('password/{id_user}', 'api\v2\mitra\AuthController@changePassword');
            Route::post('validate', 'api\v2\mitra\AuthController@validateUser');
        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('/{id_merchant}', 'api\v2\mitra\ProdukController@byMerchant');
            Route::post('/', 'api\v2\mitra\ProdukController@store');
            Route::post('/{id_product}', 'api\v2\mitra\ProdukController@update');
        });

        Route::group(['prefix' => 'category'], function () {
            Route::get('/', 'api\v2\mitra\KategoriController@index');
            Route::post('/', 'api\v2\mitra\KategoriController@store');
        });

        Route::group(['prefix' => 'brand'], function () {
            Route::get('/{id_merchant}', 'api\v2\mitra\BrandController@brandByMerchant');
            Route::post('/', 'api\v2\mitra\BrandController@store');
        });

        Route::group(['prefix' => 'merchant'], function () {
            Route::get('/', 'api\v2\mitra\MerchantController@index');
            Route::get('/{id_merchant}', 'api\v2\mitra\MerchantController@show');
            Route::get('panel/{id_merchant}', 'api\v2\mitra\MerchantController@merchantPanel');
            Route::get('confirm/{id_merchant}', 'api\v2\mitra\MerchantController@merchantConfirmList');
            Route::post('confirm', 'api\v2\mitra\MerchantController@merchantConfirm');
            Route::get('history/{id_merchant}', 'api\v2\mitra\MerchantController@merchantHistory');
            Route::get('history/qrcode/{id_merchant}', 'api\v2\mitra\MerchantController@qrcodeHistory');
            Route::post('confirm/cancel', 'api\v2\mitra\MerchantController@confirmCancel');
            Route::post('profile/{id_merchant}', 'api\v2\mitra\MerchantController@updateProfile');
        });

        Route::group(['prefix' => 'courier'], function () {
            Route::post('confirm', 'api\v2\mitra\CourierController@courierConfirm');
            Route::post('confirm/all', 'api\v2\mitra\CourierController@courierConfirmAll');
            Route::get('confirm/{id_courier}', 'api\v2\mitra\CourierController@courierConfirmList');
            Route::get('history/{id_courier}', 'api\v2\mitra\CourierController@courierHistory');
            Route::get('merchant/{id_merchant}', 'api\v2\mitra\CourierController@courierByMerchant');
            Route::post('/{id_courier}', 'api\v2\mitra\CourierController@changeCourierActive');
        });

        Route::group(['prefix' => 'region'], function () {
            Route::get('/', 'api\v2\mitra\RegionController@districtAll');
            Route::get('merchant/{id_merchant}', 'api\v2\mitra\RegionController@showMerchantRegion');
            Route::post('merchant', 'api\v2\mitra\RegionController@createMerchantRegion');
            Route::delete('merchant/{id}', 'api\v2\mitra\RegionController@deleteMerchantRegion');
        });

        Route::group(['prefix' => 'stock'], function () {
            Route::post('/', 'api\v2\mitra\StokController@store');
            Route::post('/return', 'api\v2\mitra\StokController@returnStock');
        });
    });
});
