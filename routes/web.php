<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product/image={image_code}', 'MainController@imageOriginal');
Route::get('product/thumbnail={image_code}', 'MainController@imageThumbnail');
Route::get('banner={image_code}', 'MainController@imageBanner');
Route::get('merchant/image={image_code}', 'MainController@imageMerchant');

//Generate QR Code
Route::get('generate/qrcode/{qrcode}', 'MainController@generateQrcodeMypay');
Route::get('generate/qrcode/linkaja/{qrcode}', 'MainController@generateQrcodeLinkaja');
